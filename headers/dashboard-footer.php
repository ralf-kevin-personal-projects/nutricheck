
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="../assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="../assets/plugins/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="../assets/js/main-js.js"></script>

    <!-- chart chartist js -->
    <!-- <script src="../assets/plugins/charts/chartist-bundle/chartist.min.js"></script> -->

    <!-- sparkline js -->
    <!-- <script src="../assets/plugins/charts/sparkline/jquery.sparkline.js"></script> -->

    <!-- morris js -->
    <!-- <script src="../assets/plugins/charts/morris-bundle/raphael.min.js"></script>
    <script src="../assets/plugins/charts/morris-bundle/morris.js"></script> -->

    <!-- chart c3 js -->
    <!-- <script src="../assets/plugins/charts/c3charts/c3.min.js"></script>
    <script src="../assets/plugins/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="../assets/plugins/charts/c3charts/C3chartjs.js"></script>
    <script src="../assets/js/dashboard-ecommerce.js"></script> -->

    <!-- datatables js -->
    <script type="text/javascript" src="../assets/plugins/datatables/DataTables-1.10.18/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/AutoFill-2.3.2/js/dataTables.autoFill.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/Buttons-1.5.4/js/dataTables.buttons.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/Responsive-2.2.2/js/dataTables.responsive.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/RowGroup-1.1.0/js/dataTables.rowGroup.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/Scroller-1.5.0/js/dataTables.scroller.js"></script>

    <script type="text/javascript" src="../assets/js/jquery.canvasjs.min.js"></script>

    <!-- Exporting tables -->
    <script type="text/javascript" src="../assets/plugins/datatables/jszip.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/pdfmake.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/vfs_fonts.js"></script>

    <script type="text/javascript" src="../assets/plugins/datatables/Buttons-1.5.4/js/buttons.html5.js"></script>
    <script type="text/javascript" src="../assets/plugins/datatables/Buttons-1.5.4/js/buttons.print.js"></script>    
    <!-- <script type="text/javascript" src="../assets/plugins/charts/chartist-bundle/chartist.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/charts/chartist-bundle/Chartistjs.js"></script> -->

    <!-- <script type="text/javascript" src="../assets/js/canvasjs.min.js"></script> -->
</body>
 
</html>


<script>

    $(document).ready(function() {

        checkSessions();

        $("#project").click(function() {
            location.href = "project-performance.php";
        }); 
        
        $("#parish").click(function() {
            location.href = "parish-performance.php";
        });

        $("#site").click(function() {
            location.href = "site-performance.php";
        });        

        $("#feeding").click(function() {
            location.href = "feeding-site.php";
        });

        $("#coordinator").click(function() {
            location.href = "coordinator.php";
        });

        $("#userlogs").click(function() {
            location.href = "userlogs.php";
        });
        
        $("#graduates").click(function() {
            location.href = "graduates.php";
        });        

        $("#logout").click(function(){
            if(confirm("Are you sure you want to Logout?")){
                logout();
            }
        });

        function checkSessions() {
            if (typeof(Storage) !== "undefined") {

                if (localStorage.length < 0 || localStorage.length == 0) {

                    location.href = "../";

                } else {

                    if (localStorage.getItem("user") == null || localStorage.getItem("user") == "") {

                        location.href = "../";

                    } else {
                        var name = "Welcome, " + localStorage.getItem("fullname") + " ";
                        $("#user_name").text(name)
                        $("#user_name2").text(name)

                    }
                }

            } else {

                alert("Your Browser is not supported to our function, please download better browser")
                location.href = "../";

            }
        }        
        // project
        // parish
        // site
        // benchmark
        // trends
        // comparison
        // 
        // 

        function logout() {

            var fd = new FormData();

            fd.append("log_id", localStorage.getItem("log_id"));
            fd.append("request", "log_out");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) 
                    {

                        window.localStorage.removeItem("id");
                        window.localStorage.removeItem("email");
                        window.localStorage.removeItem("fullname");
                        window.localStorage.removeItem("user");
                        window.localStorage.removeItem("log_id");
                        location.href = "../";
                        
                    } else {
                        alert("Warning: logout not monitored");

                        window.localStorage.removeItem("id");
                        window.localStorage.removeItem("email");
                        window.localStorage.removeItem("fullname");
                        window.localStorage.removeItem("user");
                        window.localStorage.removeItem("log_id");
                        location.href = "../";
                    }
                
                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }

            });
        }

    });

</script>