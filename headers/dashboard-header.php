<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css">
    <link href="../assets/plugins/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/plugins/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="../assets/plugins/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="../assets/plugins/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="../assets/plugins/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../assets/plugins/charts/c3charts/c3.css">
    <link rel="stylesheet" href="../assets/plugins/fonts/flag-icon-css/flag-icon.min.css">

    <link rel="stylesheet" href="../assets/css/dashboard.css">

    <!-- datatables css -->
    <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/DataTables-1.10.18/css/jquery.dataTables.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/AutoFill-2.3.2/css/autoFill.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/Buttons-1.5.4/css/buttons.dataTables.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/Responsive-2.2.2/css/responsive.dataTables.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/RowGroup-1.1.0/css/rowGroup.dataTables.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/plugins/datatables/Scroller-1.5.0/css/scroller.dataTables.css"/>


    <title>Nutricheck - Admin</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="#">NUTRICHECK ADMIN</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="../assets/img/logo.png" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 id="user_name" class="mb-0 text-white nav-user-name"></h5>
                                </div>
                                <!-- <a class="dropdown-item" href="#"><i class="fas fa-user mr-2"></i>Account</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>Setting</a> -->
                                <a id="logout" class="dropdown-item" href="#"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                <h4 id="user_name2"></h4>
                                Menu
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link " href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1">
                                    <i class="fa fa-fw fa-list"></i>
                                        PM Reports <span class="badge badge-success">6
                                </span></a>
                                <div id="submenu-1" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a id="project" class="nav-link">Project Performance</a>
                                        </li>
                                        <!-- <li class="nav-item">
                                            <a id="parish" class="nav-link">Parish Performance</a>
                                        </li> -->
                                        <li class="nav-item">
                                            <a id="site" class="nav-link">Site Performance</a>
                                        </li>                                        
                                    </ul>
                                </div>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2">
                                    <i class="fa fa-fw fa-rocket"></i>
                                Operational Reports</a>

                                <div id="submenu-2" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a id="benchmark" class="nav-link">Benchmark</a>
                                        </li>
                                        <li class="nav-item">
                                            <a id="trends" class="nav-link">Trends</a>
                                        </li>
                                        <li class="nav-item">
                                            <a id="comparison" class="nav-link">Comparison</a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3">
                                    <i class="fas fa-fw fa-folder"></i>
                                    Data Entry</a>
                                <div id="submenu-3" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a id="feeding" class="nav-link">Feeding Site</a>
                                        </li>
                                        <li class="nav-item">
                                            <a id="coordinator" class="nav-link">Coordinator</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a id="graduates" class="nav-link" href="#">
                                    <i class="fas fa-fw fa-list"></i>
                                    Candidate for Graduation</a>
                            </li>

                            <li class="nav-item">
                                <a id="userlogs" class="nav-link" href="#">
                                    <i class="fas fa-fw fa-cog"></i>
                                    User Logs</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
