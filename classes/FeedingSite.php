<?php

class FeedingSite extends Database {


    private $site;
    private $parish;
    private $date_started;
    private $date_finish;
    private $status;
    private $coord;
    private $coord_id;

    private $id;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {

            case "create_fs":
                $this->site = $args["site"];
                $this->parish = $args["parish"];
                $this->date_started = $args["date_started"];
                $this->date_finish = $args["date_finish"];
                $this->status = $args["status"];
                $this->coord = $args["coord"];
                $this->coord_id = $args["coord_id"];
            break;
            case "update_fs":
                $this->site = $args["site"];
                $this->parish = $args["parish"];
                $this->date_started = $args["date_started"];
                $this->date_finish = $args["date_finish"];                
                $this->status = $args["status"];
                $this->coord = $args["coord"];
                $this->coord_id = $args["coord_id"];
                $this->id = $args["id"];
            break;
            case "fetch_fs":
            break;
            case "del_fs":
                $this->id = $args["id"];            
            break;            
            default:

            break;
        }
    }

    
    public function createFeedingSite() {

        $this->createConn();

        $this->query("INSERT INTO feedingsite (fs_site, fs_parish, fs_date_started, coord_fullname, coord_id, fs_status, fs_date_finish) 
                    VALUES 
                    ('". $this->site ."', '". $this->parish ."', '". $this->date_started ."', '". $this->coord ."', 
                    '". $this->coord_id ."', '". $this->status ."', '". $this->date_finish ."') ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;
    }


    public function updateFeedingSite() {

        $this->createConn();

        $this->query("UPDATE feedingsite SET
                    fs_site = '". $this->site ."', fs_parish = '". $this->parish ."', fs_date_started = '". $this->date_started ."', 
                    coord_fullname = '". $this->coord ."', coord_id = '". $this->coord_id ."', fs_status = '". $this->status ."',
                    fs_date_finish = '". $this->date_finish ."'
                    WHERE fs_id = '". $this->id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = "No other changes were detected";

        }
    
        return $this->res;
    }


    public function delete() {

        $this->createConn();

        $this->query("DELETE FROM feedingsite WHERE fs_id = '". $this->id ."' ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = "Successfully Deleted!";

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;
    }


    
    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT * FROM feedingsite");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }

}