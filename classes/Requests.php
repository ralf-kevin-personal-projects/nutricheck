<?php

require('../config.php');
require('Database.php');
require('Validation.php');
require('Account.php');
require('Coordinator.php');
require('FeedingSite.php');
require('Project.php');
require('Parish.php');
require('Logs.php');
require('Beneficiary.php');


$json_response = array();
$validation = new Validation();

if (isset($_POST["request"])) {
    $req = $_POST["request"];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
    if ($validation->checkItems($_POST)) {
        
        switch ($req) {
            case "login":
                $account = new Account($_POST, "login");
                $json_response = $account->login();
            break;
            case "create_acct":
                $account = new Account($_POST, "create_acct");
                $json_response = $account->validatedData();
            break;
            case "log_out":
                $account = new Account($_POST, "log_out");
                $json_response = $account->out();
            break;            
            case "forgot_pass":
                $account = new Account($_POST, "forgot_pass");
                $json_response = $account->forgotPass();
            break;

            

            //Project Performance
            case "count_fs":
                $project = new Project(null, "count_fs");
                $json_response = $project->countFeedingSites();
            break;
            case "count_b":
                $project = new Project($_POST, "count_b");
                $json_response = $project->countBeneficiaries();
            break;
            case "fetch_ps":
                $project = new Project($_POST, "fetch_ps");
                $json_response = $project->fetchPS();
            break;
            case "fetch_age":
                $project = new Project($_POST, "fetch_age");
                $json_response = $project->fetchAge();
            break;
            case "fetch_weight":
                $project = new Project($_POST, "fetch_weight");
                $json_response = $project->fetchWeight();
            break;

            //Parish Performance
            case "fetch_parish":
                $parish = new Parish(null, "fetch_parish");
                $json_response = $parish->fetchParish();
            break;
            case "parish_perf":
                $parish = new Parish($_POST, "parish_perf");
                $json_response = $parish->parishPerformance();
            break;

            //Coordinator
            case "fetch_coord":
                $coord = new Coordinator(null, "fetch_coord");
                $json_response = $coord->fetchAll();
            break;
            case "create_coord":
                $coord = new Coordinator($_POST, "create_coord");
                $json_response = $coord->createCoord();
            break;
            case "update_coord":
                $coord = new Coordinator($_POST, "update_coord");
                $json_response = $coord->updateCoord();
            break;
            case "del_coord":
                $coord = new Coordinator($_POST, "del_coord");
                $json_response = $coord->delete();
            break;
            


            //Feeding Site
            case "fetch_fs":
                $fs = new FeedingSite(null, "fetch_fs");
                $json_response = $fs->fetchAll();
            break;            
            case "create_fs":
                $fs = new FeedingSite($_POST, "create_fs");
                $json_response = $fs->createFeedingSite();
            break;
            case "update_fs":
                $fs = new FeedingSite($_POST, "update_fs");
                $json_response = $fs->updateFeedingSite();
                // print_r($_POST);
            break;
            case "del_fs":
                $fs = new FeedingSite($_POST, "del_fs");
                $json_response = $fs->delete();
                // print_r($_POST);
            break;            


            //beneficiary
            case "candidates":
                $b = new Beneficiary($_POST, "candidates");
                $json_response = $b->candidates();
            break;            

            //User logs
            case "fetch_log":
                $logs = new Logs(null, "fetch_log");
                $json_response = $logs->fetchAll();
            break;

            default:
                $json_response["success"] = false;
                $json_response["result"] = "Unknown Request";
            break;
        }
        
    } else {
        $json_response["success"] = false;
        $json_response["result"] = "Empty Post Value";        
    }

} else {
    $json_response["success"] = false;
    $json_response["result"] = "Empty Request Value";
}

echo json_encode($json_response);