<?php

class Project extends Database {


    private $site;
    private $parish;
    private $date_started;
    private $date_finish;
    private $status;
    private $coord;
    private $coord_id;

    private $id;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {

            case "fetch_fs":
            break;
            case "fetch_ps":
            break;
            case "count_fs":
            break;
            case "fetch_age":
            break;
            case "fetch_weight":
            break;
            default;

            break;
        }
    }


    public function countFeedingSites() {

        $this->createConn();

        $this->query("SELECT COUNT(*) as count FROM feedingsite");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }



    public function countBeneficiaries() {

        $this->createConn();

        $this->query("SELECT COUNT(*) as count,
                    IFNULL(SUM(CASE WHEN b_gender = 'Male' THEN 1 END), 0) as male,
                    IFNULL(SUM(CASE WHEN b_gender = 'Female' THEN 1 END), 0) as female
                    FROM beneficiary");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }


    public function fetchPS() {

        $this->createConn();

        $this->query("SELECT fs.fs_id, fs.fs_site, fs.fs_date_started, fs.fs_date_finish,
                        COUNT(b.b_id) as count_b, 
                        IFNULL(SUM(CASE WHEN b.b_gender = 'Male' THEN 1 END), 0) as male, 
                        IFNULL(SUM(CASE WHEN b.b_gender = 'Female' THEN 1 END), 0) as female FROM feedingsite as fs 
                    LEFT JOIN beneficiary as b ON fs.fs_id = b.fs_id
                    WHERE fs.fs_status = 'Active'
                    GROUP BY fs.fs_id, fs.fs_site");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }
   

    public function fetchAge() {

        $this->createConn();

        $this->query("SELECT b_gender,b_age, count(*) as count FROM beneficiary GROUP BY b_age, b_gender ORDER BY b_age ASC");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
     
        return $this->res;

    }

    public function fetchWeight() {

        $this->createConn();

        $this->query("SELECT b_weight_new, b_bmi_status_new, count(*) as count 
                    FROM beneficiary 
                    GROUP BY b_weight_new, b_bmi_status_new ORDER BY b_weight_new ASC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
     
        return $this->res;

    }


}