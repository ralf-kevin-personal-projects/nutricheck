<?php

class Coordinator extends Database {


    private $fullname;
    private $gender;
    private $status;
    private $user;
    private $pass;
    private $hashedPass;

    private $id;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {

            case "create_coord":
                $this->fullname = $args["fullname"];
                $this->gender = $args["gender"];
                $this->status = $args["status"];
                $this->user = $args["user"];
                $this->pass = $args["pass"];
            break;
            case "update_coord":
                $this->fullname = $args["fullname"];
                $this->gender = $args["gender"];
                $this->status = $args["status"];
                $this->id = $args["id"];
            break;
            case "fetch_coord":
            break;
            case "del_coord":
                $this->id = $args["id"];            
            break;            
            default:

            break;
        }
    }

    
    public function createCoord() {

        $this->createConn();

        $this->hashedPass = md5($this->pass);

        $this->query("INSERT INTO coordinator (coord_fullname, coord_gender, coord_status, coord_user, coord_pass) 
                    VALUES ('". $this->fullname ."', '". $this->gender ."', '". $this->status ."', 
                    '". $this->user ."', '". $this->hashedPass ."') ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;
    }


    public function updateCoord() {

        $this->createConn();

        $this->query("UPDATE coordinator 
                    SET coord_fullname = '". $this->fullname ."', coord_gender = '". $this->gender ."', coord_status = '". $this->status ."'
                    WHERE coord_id = '". $this->id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = "No other changes were detected";

        }
    
        return $this->res;
    }


    public function delete() {

        $this->createConn();

        $this->query("DELETE FROM coordinator WHERE coord_id = '". $this->id ."' ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = "Successfully Deleted!";

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;
    }


    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT * FROM coordinator");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }

}