<?php

class Parish extends Database {

    private $id;
    private $date_started;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {

            case "fetch_fs":
            break;
            case "fetch_parish":
            break;
            case "parish_perf":
                $this->id = $args["id"];
                $this->date_started = $args["date_started"];
            break;
            default:

            break;
        }
    }

    
    public function fetchParish() {

        $this->createConn();

        $this->query("SELECT fs_id, fs_site FROM feedingsite");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }

    public function parishPerformance() {

        $this->createConn();

        $this->query("SELECT 
                        fs.fs_id,
                        fs.fs_parish,
                        fs.fs_date_started,
                        fs.fs_date_finish,
                        COUNT(b.b_id) as count_b,
                        IFNULL(SUM(CASE WHEN b.b_gender = 'Male' THEN 1 END), 0) as male,
                        IFNULL(SUM(CASE WHEN b.b_gender = 'Female' THEN 1 END), 0) as female
                    
                    FROM
                        feedingsite as fs
                    INNER JOIN
                        beneficiary as b
                    ON
                        fs.fs_id = b.fs_id
                        
                    WHERE
                        fs.fs_id = '". $this->id ."' AND fs.fs_date_started = '". $this->date_started ."' ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }


    public function countFeedingSites() {

        $this->createConn();

        $this->query("SELECT COUNT(*) as count FROM feedingsite");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }


    // to be continue
    public function countBeneficiaries() {

        $this->createConn();

        $this->query("SELECT COUNT(*) as count,
                    IFNULL(SUM(CASE WHEN b_gender = 'Male' THEN 1 END), 0) as male,
                    IFNULL(SUM(CASE WHEN b_gender = 'Female' THEN 1 END), 0) as female
                    FROM beneficiary");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }
   

}