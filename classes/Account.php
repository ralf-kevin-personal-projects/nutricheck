<?php

//Mailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Mailer
require('PHPMailer/src/Exception.php');
require('PHPMailer/src/PHPMailer.php');
require('PHPMailer/src/SMTP.php');

class Account extends Database {

    private $id;
    private $email;
    private $fullname;
    private $user;
    private $pass;

    private $hashPass;

    private $log_id;
    private $log_fullname;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "login":
                $this->user = $args["user"];
                $this->pass = $args["pass"];
            break;
            case "create_acct":
                $this->email = $args["email"];
                $this->fullname = $args["fullname"];
                $this->user = $args["user"];
                $this->pass = $args["pass"];                
            break;
            case "log_out":
                $this->log_id = $args["log_id"];
            break;
            case "forgot_pass":
                $this->email = $args["email"];                
            break;
            default:

            break;
        }
    }


    public function login() {
        
        $this->createConn();

        $this->hashPass = md5($this->pass);

        $this->query("SELECT
                    id, email, fullname, user
                    FROM admin 
                    WHERE user = '". $this->user ."' AND pass = '". $this->hashPass ."' ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            //for userlogs

            //gets the fullname
            $this->log_fullname = $hasResult["result"][0]["fullname"];

            //execute log func
            $log_in = $this->in();

            if($log_in["success"] == true) {
                $this->res["success"] = true;
                $this->res["result"] = 
                        array("user" => $hasResult["result"], 
                        "log_id" => $log_in["result"]);
            } else {

                $this->res["success"] = true;
                $this->res["result"] = $hasResult["result"];
            }

        } else {

            $this->res["success"] = false;
            $this->res["result"] = "Account does not Exist";

        }
    
        return $this->res;
    }
    
    public function createAccount() {

        $this->createConn();

        $this->hashPass = md5($this->pass);

        $this->query("INSERT INTO admin (email, fullname, user, pass) 
                    VALUES ('". $this->email ."', '". $this->fullname ."', '". $this->user ."', '". $this->hashPass ."') ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = "Successfully Created";

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;
    }


    public function validatedData() {

                
        $this->createConn();

        $this->query(" SELECT user FROM admin WHERE user = '". $this->user ."' ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = "This username already taken";

        } else {
            $this->res = $this->createAccount();
        }
    
        return $this->res;
        
    }


    private function in() {
        date_default_timezone_set('Asia/Manila');
        $in = date("h:i:s a");

        $this->createConn();

        $this->query("INSERT INTO logs (log_fullname, log_role, log_in) 
                    VALUES ('". $this->log_fullname ."', 'Admin', '". $in ."') ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $this->getLastInsertedID();

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;
    }

    public function out() {

        date_default_timezone_set('Asia/Manila');
        $out = date("h:i:s a");

        $this->createConn();

        $this->query("UPDATE logs SET log_out = '". $out ."' WHERE log_id = '". $this->log_id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
    
        return $this->res;

    }




    public function forgotPass() {

        $this->createConn();


        $this->query("SELECT id FROM admin WHERE email = '". $this->email ."' ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->id = $hasResult["result"][0]["id"];
            $this->sendEmail();
        } else {

            $this->res["success"] = false;
            $this->res["result"] = "Please check your inbox for our verification";

        }

        return $this->res;
    }

    private function sendEmail() {

        $body = "<h3> Please go to this link to reset your password </h3>
                <div>
                    <a href='http://localhost/nutricheck-reset/index.php?ref=". $this->id ." &type=admin'> http://localhost/nutricheck-reset/ </a>
                </div>";

        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'nutricheck2468@gmail.com';                 // SMTP username
            $mail->Password = 'nutri.check2468';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to
        
            //Recipients
            $mail->setFrom('nutricheck2468@gmail.com', 'Nutricheck Reset Password');
            $mail->addAddress($this->email);     // Add a recipient
            // $mail->addAddress('ellen@example.com');               // Name is optional
            $mail->addReplyTo('nutricheck2468@gmail.com', 'Nutricheck');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');
        
            //Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        
            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'RESET PASSWORD';
            $mail->Body    = $body;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
            
            if ($mail->send()) {
                $this->res["success"] = true;
                $this->res["result"] = 'Message has been sent';
    
            } else {
                $this->res["success"] = false;
                $this->res["result"] = 'Message not sent';
    
            }



            // echo 'Message has been sent';

        } catch (Exception $e) {

            $this->res["success"] = false;
            $this->res["result"] = 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo;

            // $res = echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }        


        return $this->res;        

    }

}