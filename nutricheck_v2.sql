-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2019 at 08:44 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nutricheck_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `user` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `fullname`, `user`, `pass`, `date_created`) VALUES
(1, 'nutri@gmail.com', 'Admin', 'user', '1a1dc91c907325c69271ddf0c944bc72', '2018-12-27 23:09:02');

-- --------------------------------------------------------

--
-- Table structure for table `beneficiary`
--

CREATE TABLE `beneficiary` (
  `b_id` int(11) NOT NULL,
  `b_fname` varchar(100) NOT NULL,
  `b_mname` varchar(100) NOT NULL,
  `b_lname` varchar(100) NOT NULL,
  `b_dob` varchar(100) NOT NULL,
  `b_parish` varchar(200) NOT NULL,
  `b_address` varchar(200) NOT NULL,
  `b_height` varchar(50) NOT NULL,
  `b_weight` varchar(50) NOT NULL,
  `b_bmi_status` varchar(50) NOT NULL,
  `b_gender` varchar(20) NOT NULL,
  `b_height_new` varchar(50) NOT NULL,
  `b_weight_new` varchar(50) NOT NULL,
  `b_bmi_status_new` varchar(50) NOT NULL,
  `b_date_started` varchar(50) NOT NULL,
  `b_date_finish` varchar(50) NOT NULL,
  `fs_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beneficiary`
--

INSERT INTO `beneficiary` (`b_id`, `b_fname`, `b_mname`, `b_lname`, `b_dob`, `b_parish`, `b_address`, `b_height`, `b_weight`, `b_bmi_status`, `b_gender`, `b_height_new`, `b_weight_new`, `b_bmi_status_new`, `b_date_started`, `b_date_finish`, `fs_id`) VALUES
(1, 'Jennelyn', 'Foncesca', 'Hayuhay', '1995-03-02', 'parish3333', 'sample address', '150', '45', 'Normal', 'Female', '150', '46.5', 'Normal', '2019-01-06', '2019-7-06', 3);

-- --------------------------------------------------------

--
-- Table structure for table `beneficiary_bmi`
--

CREATE TABLE `beneficiary_bmi` (
  `bmi_id` int(11) NOT NULL,
  `bmi_month` varchar(50) NOT NULL,
  `bmi_height` varchar(50) NOT NULL,
  `bmi_weight` varchar(50) NOT NULL,
  `bmi_status` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `b_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `beneficiary_bmi`
--

INSERT INTO `beneficiary_bmi` (`bmi_id`, `bmi_month`, `bmi_height`, `bmi_weight`, `bmi_status`, `date_created`, `b_id`) VALUES
(1, 'January', '150', '45.5', 'Normal', '2019-01-07 01:14:32', 1),
(2, 'February', '150', '45.7', 'Normal', '2019-01-08 01:59:24', 1),
(3, 'March', '150', '46.5', 'Normal', '2019-01-08 02:01:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coordinator`
--

CREATE TABLE `coordinator` (
  `coord_id` int(11) NOT NULL,
  `coord_fullname` varchar(200) NOT NULL,
  `coord_role` varchar(100) NOT NULL,
  `coord_gender` varchar(30) NOT NULL,
  `coord_status` varchar(30) NOT NULL DEFAULT 'Activated',
  `coord_user` varchar(200) NOT NULL,
  `coord_pass` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coordinator`
--

INSERT INTO `coordinator` (`coord_id`, `coord_fullname`, `coord_role`, `coord_gender`, `coord_status`, `coord_user`, `coord_pass`, `date_created`) VALUES
(1, 'coord fullname', 'coordinator', 'Male', 'Deactivated', '', '', '2018-12-28 01:39:39'),
(2, 'nino villariza', 'coordinator', 'Male', 'Deactivated', '', '', '2018-12-28 01:41:04'),
(3, 'asas', 'admin', 'Male', 'Deactivated', '', '', '2018-12-28 10:02:19'),
(4, 'sample fullname', 'Marksman', 'Male', 'Activated', 'user', '1a1dc91c907325c69271ddf0c944bc72', '2019-01-05 17:29:04'),
(5, 'ralfkevin', 'Coord', 'Male', 'Activated', 'coord1', '1a1dc91c907325c69271ddf0c944bc72', '2019-01-08 18:07:50');

-- --------------------------------------------------------

--
-- Table structure for table `feedingsite`
--

CREATE TABLE `feedingsite` (
  `fs_id` int(11) NOT NULL,
  `fs_site` varchar(200) NOT NULL,
  `fs_parish` varchar(200) NOT NULL,
  `fs_status` varchar(50) NOT NULL DEFAULT 'Active',
  `fs_date_started` varchar(50) NOT NULL,
  `fs_date_finish` varchar(50) NOT NULL,
  `coord_fullname` varchar(200) NOT NULL,
  `coord_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedingsite`
--

INSERT INTO `feedingsite` (`fs_id`, `fs_site`, `fs_parish`, `fs_status`, `fs_date_started`, `fs_date_finish`, `coord_fullname`, `coord_id`, `date_created`) VALUES
(1, 'sitename1', 'parish1', 'Active', '2019-01-05', '2019-7-05', 'nino', 2, '2018-12-28 18:45:46'),
(2, 'sitename2', 'parish2', 'Deactive', '2018-12-10', '', 'coord', 1, '2018-12-28 19:05:22'),
(3, 'sitename33333', 'parish3333', 'Active', '2019-01-01', '2019-7-01', 'nino', 2, '2018-12-28 19:13:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beneficiary`
--
ALTER TABLE `beneficiary`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `beneficiary_bmi`
--
ALTER TABLE `beneficiary_bmi`
  ADD PRIMARY KEY (`bmi_id`);

--
-- Indexes for table `coordinator`
--
ALTER TABLE `coordinator`
  ADD PRIMARY KEY (`coord_id`);

--
-- Indexes for table `feedingsite`
--
ALTER TABLE `feedingsite`
  ADD PRIMARY KEY (`fs_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `beneficiary`
--
ALTER TABLE `beneficiary`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `beneficiary_bmi`
--
ALTER TABLE `beneficiary_bmi`
  MODIFY `bmi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coordinator`
--
ALTER TABLE `coordinator`
  MODIFY `coord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `feedingsite`
--
ALTER TABLE `feedingsite`
  MODIFY `fs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
