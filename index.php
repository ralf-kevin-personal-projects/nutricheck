<?php include 'headers/login-header.php' ?>
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="splash-container">
        <div class="login-form">
            <div class="card">
                <div class="card-header text-center">
                    <a href="#"><img class="logo" src="assets/img/logo.png" alt="logo"></a>
                    <span class="splash-description">Nutricheck Admin</span>
                </div>
                <div class="card-body">
                    <!-- <form> -->
                        <div class="form-group">
                            <input class="form-control form-control-lg" id="username" type="text" placeholder="Username" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <input class="form-control form-control-lg" id="password" type="password" placeholder="Password">
                        </div>

                        <button id="btn-sign-in" class="btn btn-danger btn-lg btn-block">Sign in</button>
                        <button id="btn-forgot" class="btn btn-primary btn-sm btn-block">Forgot Password</button>                                                
                    <!-- </form> -->
                </div>
                <div class="card-footer bg-white p-0  ">
                    <div class="card-footer-item card-footer-item-bordered">
                        <a id="sign-up" class="footer-link">Sign Up</a></div>
                    <!-- <div class="card-footer-item card-footer-item-bordered">
                        <a href="#" class="footer-link">Forgot Password</a>
                    </div> -->
                </div>
            </div>
        </div>        
    </div>
  
    <!-- ============================================================== -->
    <!-- end login page  -->

<!-- Modal -->
<div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="signUpModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="signUpModalTitle">Sign Up</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">
            
                <h3>User Information</h3>
                <div class="row">
                    <div class="col-md-12">
                        <label>Email</label>                                                                
                        <div class="form-group">
                            <input id="email" type="text" class="form-control" placeholder="Email"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Fullname</label>                                                                
                        <div class="form-group">
                            <input id="fullname" type="text" class="form-control" placeholder="Fullname"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Username</label>                                                                
                        <div class="form-group">
                            <input id="user" type="text" class="form-control" placeholder="Username"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Password</label>                                                                
                        <div class="form-group">
                            <input id="pass" type="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Confirm Password</label>                                                                
                        <div class="form-group">
                            <input id="cpass" type="password" class="form-control" placeholder="Confirm Password"/>
                        </div>
                    </div>

                </div>

            </div>
            <hr/>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal" onclick=location.reload()>Close</button>
        <button id="btnSave" type="button" class="btn btn-success"></button>
      </div>
    </div>
  </div>
</div>

<!-- forgot pass -->
<div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="resetModalTitle">Reset Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                    <label>Email</label>                                                                
                    <div class="form-group">
                        <input id="reset-email" type="text" class="form-control" placeholder="Email"/>
                    </div>
                </div>
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal" onclick=location.reload()>Close</button>
        <button id="btnReset" type="button" class="btn btn-success"></button>
      </div>
    </div>
  </div>
</div>




<?php include 'headers/login-footer.php' ?>

<script>

    $(document).ready(function() {

        init();

        //login
        $("#username").change(function(){
            if (!isValid(this.value)) {
                alert("username is invalid, please check your format");
                this.value = "";
                this.focus();
            }
        });

        $("#password").change(function(){
            if (!isValid(this.value)) {
                alert("password is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        // create account
        $("#email").change(function(){
            if (!isValidEmail(this.value)) {
                alert("email is invalid, please check your format");
                this.value = "";
                this.focus();
            }
        });

        $("#fullname").change(function(){
            if (!isValid(this.value)) {
                alert("fullname is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#user").change(function(){
            if (!isValid(this.value)) {
                alert("username is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#pass").change(function(){
            if (!isValid(this.value)) {
                alert("password is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#cpass").change(function(){
            if (!isValid(this.value)) {
                alert("confirm password is invalid, please check your format");
                this.value = "";
                this.focus();
            }
        });        

        // reset-email
        $("#reset-email").change(function(){
            if (!isValidEmail(this.value)) {
                alert("email is invalid, please check your format");
                this.value = "";
                this.focus();
            }
        });        

        $("#btn-sign-in").click(function(){
            var user_key = "#username";
            var pass_key = "#password";

            var user = $(user_key).val();
            var pass = $(pass_key).val();

            var values = [user, pass];
            var keys = [user_key, pass_key];

            if (validateItems(values, keys)) {
                accounts(values, "login");
            } else {
                alert("Please input all the empty fields");
            }

        });

        $("#sign-up").click(function(){
            $("#btnSave").text("Sign Up");
            $("#signUpModalTitle").text("Sign Up");
            $("#signUpModal").modal("show");
        });

        $("#btnSave").click(function(){
            var email_key = "#email";
            var fullname_key = "#fullname";
            var user_key = "#user";
            var pass_key = "#pass";
            var cpass_key = "#cpass";

            var email = $(email_key).val();
            var fullname = $(fullname_key).val();
            var user = $(user_key).val();
            var pass = $(pass_key).val();
            var cpass = $(cpass_key).val();

            var values = [email, fullname, user, pass, cpass];
            var keys = [email_key, fullname_key, user_key, pass_key, cpass_key];

            if (validateItems(values, keys)) {
                if (pass == cpass) {
                    accounts(values, "create_acct");
                } else {
                    alert("Password does not match !")
                }
            } else {
                alert("Please input all the empty fields");
            }

        });

        $("#btn-forgot").click(function(){

            $("#resetModalTitle").text("Forgot Password");            
            $("#btnReset").text("Reset Password");
            $("#resetModal").modal("show");
        });

        $("#btnReset").click(function(){
            var email_key = "#reset-email";

            var email = $(email_key).val();

            var values = [email];
            var keys = [email_key];

            if (validateItems(values, keys)) {
                accounts(values, "forgot_pass");
            } else {
                alert("Please input all the empty fields");
            }
        });

        function accounts(params, request) {

            var fd = new FormData();

            switch(request) {
                case "create_acct":
                    fd.append("email", params[0]);
                    fd.append("fullname", params[1]);
                    fd.append("user", params[2]);
                    fd.append("pass", params[3]);
                    fd.append("request", "create_acct");                
                break;
                case "login":
                    fd.append("user", params[0]);
                    fd.append("pass", params[1]);
                    fd.append("request", "login");                
                break;
                case "forgot_pass":
                    fd.append("email", params[0]);
                    fd.append("request", "forgot_pass");                
                break;
            }



            $.ajax({
                type: "POST",
                url: "classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    switch (request) {
                        case "create_acct":
                            alert(res.result);                        
                        break;
                        case "login":
                            if (res.success == true) {
                                if (typeof(Storage) !== "undefined") {

                                localStorage.setItem("id", res.result["user"][0]["id"]);
                                localStorage.setItem("email", res.result["user"][0]["email"]);
                                localStorage.setItem("fullname", res.result["user"][0]["fullname"]);
                                localStorage.setItem("user", res.result["user"][0]["user"]);
                                localStorage.setItem("log_id", res.result["log_id"]);

                                location.href = "pages/project-performance.php";

                                } else {

                                alert("Your Browser is not supported to our function, please download better browser")

                                }
                            } else {
                                alert(res.result);
                            }
                            
                            // console.log(res.result);                                                   
                            // console.log(res.result["user"][0]["email"]);
                            // console.log(res.result["log_id"]);
                        break;
                        case "forgot_pass":
                            alert(res.result);                        
                        break;
                    }

                
                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function validateItems(values, keys) {
            var isNotEmpty = false;
            var cTrue = 0;
            var cFalse = 0;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");

                    cFalse++;
                } else {
                    if (isValid(values[i])) {
                        cTrue++;
                    } else {
                        cFalse++;                        
                    }
                }
            }

            if (cTrue > cFalse) {
                isNotEmpty = true;
            } else {
                isNotEmpty = false;
            }

            return isNotEmpty;
        }

        function init() {
            $(".forgot-pass").hide();
        }

        function isValid(param) {
            var strReg = new RegExp("^[A-Za-z0-9]+$");
            if (strReg.test(param)) {
                return true;
            } else {
                return false;
            }

        }

        function isValidEmail(param) {            
            var strReg = new RegExp("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}");
            if (strReg.test(param)) {
                return true;
            } else {
                return false;
            }            
        }

    });

</script>
