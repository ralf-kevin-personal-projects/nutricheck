<?php include '../headers/dashboard-header.php'; ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Coordinator</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Data Entry</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Coordinator</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="ecommerce-widget">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <!-- <label>Date: </label>
                                        <input type="date" class="form-control" /> -->
                                    </div>
                                    <div class="col-lg-4">
                                        <!-- <label>Parish: </label>
                                        <select class="form-control">
                                            <option>SELECT PARISH</option>
                                        </select> -->
                                    </div>
                                    <div class="col-md-3">
                                        <!-- <label>&emsp;</label> -->
                                        <button id="btn-add" class="form-control btn btn-md btn-primary">Add New Coordinator</button>
                                    </div>                                    

                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table id="tbl-coord" class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">ID No.</th>
                                                    <th class="border-0">Name</th>
                                                    <th class="border-0">User</th>
                                                    <th class="border-0">Status</th>
                                                    <th class="border-0">Date Created</th>
                                                    <th class="border-0">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- <tr>
                                                    <td>
                                                        <div class="m-r-10"><img src="assets/images/product-pic.jpg" alt="user" class="rounded" width="45"></div>
                                                    </td>
                                                    <td>Product #1 </td>
                                                    <td>id000001 </td>
                                                    <td>id000001 </td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <button id="btn-edit" class="btn btn-success btn-sm"><i class="fas fa-edit"></i></button>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button id="btn-delete" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                            </div>                                                            
                                                        </div>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>                  
                </div>



            </div>

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        Copyright © 2018 Nutricheck. All rights reserved.
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->


<!-- Modal -->
<div class="modal fade" id="cdModal" tabindex="-1" role="dialog" aria-labelledby="cdModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cdModalTitle">Add New Coordinator</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group">
            
                <h6>Coordinator Information</h6>
                <div class="row">
                    <div class="col-md-12">
                        <label>Full Name</label>                                                                
                        <div class="form-group">
                            <input id="fullname" type="text" class="form-control" placeholder="Full Name"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Gender</label>                                                                
                        <div class="form-group">
                            <select id="gender" class="form-control">
                                <option value="">SELECT GENDER</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Account Status</label>                                                                
                        <div class="form-group">
                            <select id="status" class="form-control">
                                <option value="">SELECT STATUS</option>
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Username</label>                                                                
                        <div class="form-group">
                            <input id="user" type="text" class="form-control" placeholder="Username"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Password</label>                                                                
                        <div class="form-group">
                            <input id="pass" type="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Confirm Password</label>                                                                
                        <div class="form-group">
                            <input id="cpass" type="password" class="form-control" placeholder="Confirm Password"/>
                        </div>
                    </div>
                </div>

            </div>
            <hr/>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        <button id="btnSave" type="button" class="btn btn-success">Create Company Account</button>
      </div>
    </div>
  </div>
</div>


<?php include '../headers/dashboard-footer.php'; ?>

<script>

    $(document).ready(function(){

        // barChart();
        loadData();

        $("#fullname").change(function(){
            if (!isValid(this.value)) {
                alert("fullname is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#user").change(function(){
            if (!isValid(this.value)) {
                alert("username is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#pass").change(function(){
            if (!isValid(this.value)) {
                alert("password is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#cpass").change(function(){
            if (!isValid(this.value)) {
                alert("confirm password is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });        

        $(document).on("keyup", "#user", function(){
            var user = $("#user").val();
            var hide = false;

            $("#tbl-coord").find("tr").each(function(){

                var checkItem = $(this).find("td:eq(2)").text();

                if (checkItem != null && checkItem != "") {

                    if (user == checkItem) {
                        alert("Username already taken")
                        $("#user").addClass("border-danger");
                        hide = true;
                    }
                }

            });

            if (hide) {
                $("#btnSave").hide();
            } else {
                $("#user").removeClass("border-danger");                                
                $("#btnSave").show();
            }


        });

        $(document).on("click","#btn-delete", function(){
            var id = $(this).data("id");

            coordRequest(id, "del_coord");
        });

        $(document).on("click","#btn-edit", function(){
            var id = $(this).data("id");
            var fullname = $(this).data("full");
            var status = $(this).data("status");
            var gender = $(this).data("gender");
            var user = $(this).data("user");
            var pass = $(this).data("pass");

            localStorage.setItem("coord_id", id);

            $("#fullname").val(fullname).attr("disabled", false);
            $("#status").val(status).attr("disabled", false);
            $("#gender").val(gender).attr("disabled", false);
            $("#user").val(user).attr("disabled", true);
            $("#pass").val(pass).attr("disabled", true);
            $("#cpass").val(pass).attr("disabled", true);

            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#cdModalTitle").text("Update Coordinator");
            $("#cdModal").modal("show");
        });


        $(document).on("click","#btn-view", function(){
            var id = $(this).data("id");
            var fullname = $(this).data("full");
            var role = $(this).data("role");
            var status = $(this).data("status");
            var gender = $(this).data("gender");


            $("#fullname").val(fullname).attr("disabled", true);
            $("#status").val(status).attr("disabled", true);
            $("#gender").val(gender).attr("disabled", true);

            $("#btnSave").hide();
            $("#cdModalTitle").text("Update Coordinator");
            $("#cdModal").modal("show");
        });        


        $(document).on("click","#btn-add", function(){
            $("#btnSave").text("Add New Coordinator");
            $("#btnSave").show();
            $("#cdModalTitle").text("Add New Coordinator");
            $("#cdModal").modal("show");
        });

        $("#btnSave").click(function(){
            var cmd = $(this).text();

            var fullname_key = "#fullname";
            var gender_key = "#gender";
            var status_key = "#status";
            var user_key = "#user";
            var pass_key = "#pass";
            var cpass_key = "#cpass";

            var fullname = $(fullname_key).val();
            var gender = $(gender_key).val();
            var status = $(status_key).val();
            var user = $(user_key).val();
            var pass = $(pass_key).val();
            var cpass = $(cpass_key).val();

            var values = [fullname, gender, status, user, pass, cpass];
            var keys = [fullname_key, gender_key, status_key, user_key, pass_key, cpass_key];

            switch(cmd) {
                case "Add New Coordinator":

                    if (validateItems(values, keys)) {

                        if (pass == cpass) {
                            coordRequest(values, "create_coord");
                        } else {
                            alert("Password doest not match!")
                        }

                    } else {
                        alert("Please input all the empty fields");
                    }

                break;
                case "Save Changes":
                    var fullname_key = "#fullname";
                    var gender_key = "#gender";
                    var status_key = "#status";

                    var fullname = $(fullname_key).val();
                    var gender = $(gender_key).val();
                    var status = $(status_key).val();

                    var values = [fullname, gender, status];
                    var keys = [fullname_key, gender_key, status_key];

                    if (validateItems(values, keys)) {

                        coordRequest(values, "update_coord");

                    } else {
                        alert("Please input all the empty fields");
                    }



                break;
            }



        });


        function coordRequest(values, request) {

            var fd = new FormData();

            if (request != "del_coord") {

                switch(request) {
                    case "create_coord":
                        fd.append("request", "create_coord");
                    break;
                    case "update_coord":
                        fd.append("id", localStorage.getItem("coord_id"));
                        fd.append("request", "update_coord");
                    break;
                }

                fd.append("fullname", values[0]);
                fd.append("gender", values[1]);
                fd.append("status", values[2]);
                fd.append("user", values[3]);
                fd.append("pass", values[4]);

            } else {
                fd.append("id", values);
                fd.append("request", "del_coord");
            }



            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    alert(res.result);

                    if (request == "create_coord") {
                        clearItems();                        
                    }
                    loadData();
                    localStorage.removeItem("coord_id");

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }



        function loadData() {
            var fd = new FormData();
            fd.append("request", "fetch_coord");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }


        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var id = datas[i]["coord_id"];
                    var fullname = datas[i]["coord_fullname"];
                    var role = datas[i]["coord_role"];
                    var status = datas[i]["coord_status"];
                    var gender = datas[i]["coord_gender"];
                    var date_created = datas[i]["date_created"];
                    var user = datas[i]["coord_user"];
                    var pass = datas[i]["coord_pass"];

                    tmpl += dataStatus(id, fullname, role, status, gender, date_created, user, pass);
                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-coord").find("tbody tr").remove().end();
            $("#tbl-coord").append(tmpl);
            $('#tbl-coord').DataTable();
            
        }


        function dataStatus(id, fullname, role, status, gender, date_created, user, pass) {
            var tmpl;

            if (status == "Inactive") {
                    tmpl += "<tr>"+
                            "<td class='danger'>"+ id +"</td>"+
                            "<td class='danger'>"+ fullname +"</td>"+
                            "<td class='danger'>"+ user +"</td>"+
                            "<td class='danger'>"+ status +"</td>"+
                            "<td class='danger'>"+ date_created +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-edit' class='btn btn-sm btn-success' "+
                                        "data-id='"+ id +"' "+
                                        "data-full='"+ fullname +"' "+
                                        "data-role='"+ role +"' "+
                                        "data-status='"+ status +"' "+
                                        "data-gender='"+ gender +"' "+
                                        "data-user='"+ user +"' "+
                                        "data-pass='"+ pass +"' "+
                                        "data-date='"+ date_created +"' >"+                              
                                        "<i class='fas fa-edit'></i>"+
                                    "</button> "+
                                    "<button id='btn-view' class='btn btn-sm btn-primary' "+
                                        "data-id='"+ id +"' "+
                                        "data-full='"+ fullname +"' "+
                                        "data-role='"+ role +"' "+
                                        "data-status='"+ status +"' "+
                                        "data-gender='"+ gender +"' "+
                                        "data-user='"+ user +"' "+
                                        "data-pass='"+ pass +"' "+                                        
                                        "data-date='"+ date_created +"' >"+                                    
                                        "<i class='fas fa-eye'></i>"+
                                    "</button> "+                    
                                    "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                        "data-id='"+ id +"' >"+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button> "+
                                "</div>"+
                            "</td>"+
                            "</tr>";
            } else {
                    tmpl += "<tr>"+
                            "<td>"+ id +"</td>"+
                            "<td>"+ fullname +"</td>"+
                            "<td>"+ user +"</td>"+
                            "<td>"+ status +"</td>"+
                            "<td>"+ date_created +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-edit' class='btn btn-sm btn-success' "+
                                        "data-id='"+ id +"' "+
                                        "data-full='"+ fullname +"' "+
                                        "data-role='"+ role +"' "+
                                        "data-status='"+ status +"' "+
                                        "data-gender='"+ gender +"' "+
                                        "data-user='"+ user +"' "+
                                        "data-pass='"+ pass +"' "+                                        
                                        "data-date='"+ date_created +"' >"+                              
                                        "<i class='fas fa-edit'></i>"+
                                    "</button> "+
                                    "<button id='btn-view' class='btn btn-sm btn-primary' "+
                                        "data-id='"+ id +"' "+
                                        "data-full='"+ fullname +"' "+
                                        "data-role='"+ role +"' "+
                                        "data-status='"+ status +"' "+
                                        "data-gender='"+ gender +"' "+
                                        "data-user='"+ user +"' "+
                                        "data-pass='"+ pass +"' "+                                        
                                        "data-date='"+ date_created +"' >"+                                    
                                        "<i class='fas fa-eye'></i>"+
                                    "</button> "+                    
                                    "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                        "data-id='"+ id +"' >"+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button> "+
                                "</div>"+
                            "</td>"+
                            "</tr>";                
            }

            return tmpl;
        }


        function validateItems(values, keys) {

            var isNotEmpty = false;
            var cTrue = 0;
            var cFalse = 0;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");

                    cFalse++;
                } else {

                    cTrue++;
                }
            }

            if (cTrue > cFalse) {
                isNotEmpty = true;
            } else {
                isNotEmpty = false;
            }

            return isNotEmpty;

        }

        function clearItems() {

            var fullname_key = "#fullname";
            var gender_key = "#gender";
            var status_key = "#status";
            var user_key = "#user";
            var pass_key = "#pass";
            var cpass_key = "#cpass";

            $(fullname_key).val("");
            $(gender_key).val("");
            $(status_key).val("");
            $(user_key).val("");
            $(pass_key).val("");
            $(cpass_key).val("");

        }

        function isValid(param) {
            var strReg = new RegExp("^[A-Za-z0-9]+$");
            
            if (strReg.test(param)) {
                return true;
            } else {
                return false;
            }
        }

    });

</script>