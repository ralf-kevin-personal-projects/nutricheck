<?php include '../headers/dashboard-header.php'; ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Userlogs</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Userlogs</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="ecommerce-widget">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table id="tbl-logs" class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">Log ID</th>
                                                    <th class="border-0">Fullname</th>
                                                    <th class="border-0">Role</th>
                                                    <th class="border-0">In</th>
                                                    <th class="border-0">Out</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- <tr>
                                                    <td>
                                                        <div class="m-r-10"><img src="assets/images/product-pic.jpg" alt="user" class="rounded" width="45"></div>
                                                    </td>
                                                    <td>Product #1 </td>
                                                    <td>id000001 </td>
                                                    <td>id000001 </td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <button id="btn-edit" class="btn btn-success btn-sm"><i class="fas fa-edit"></i></button>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button id="btn-delete" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                            </div>                                                            
                                                        </div>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>                  
                </div>



            </div>

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        Copyright © 2018 Nutricheck. All rights reserved.
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->


<!-- Modal -->
<div class="modal fade" id="cdModal" tabindex="-1" role="dialog" aria-labelledby="cdModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cdModalTitle">Add New Coordinator</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group">
            
                <h6>Coordinator Information</h6>
                <div class="row">
                    <div class="col-md-12">
                        <label>Full Name</label>                                                                
                        <div class="form-group">
                            <input id="fullname" type="text" class="form-control" placeholder="Full Name"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Role</label>                                                                
                        <div class="form-group">
                            <input id="role" type="text" class="form-control" placeholder="Role"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Gender</label>                                                                
                        <div class="form-group">
                            <select id="gender" class="form-control">
                                <option value="">SELECT GENDER</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Account Status</label>                                                                
                        <div class="form-group">
                            <select id="status" class="form-control">
                                <option value="">SELECT STATUS</option>
                                <option value="Activated">Activated</option>
                                <option value="Deactivated">Deactivated</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Username</label>                                                                
                        <div class="form-group">
                            <input id="user" type="text" class="form-control" placeholder="Username"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Password</label>                                                                
                        <div class="form-group">
                            <input id="pass" type="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Confirm Password</label>                                                                
                        <div class="form-group">
                            <input id="cpass" type="password" class="form-control" placeholder="Confirm Password"/>
                        </div>
                    </div>
                </div>

            </div>
            <hr/>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="btnSave" type="button" class="btn btn-success">Create Company Account</button>
      </div>
    </div>
  </div>
</div>


<?php include '../headers/dashboard-footer.php'; ?>

<script>

    $(document).ready(function(){

        // barChart();
        loadData();





        function loadData() {
            var fd = new FormData();
            fd.append("request", "fetch_log");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }


        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var log_id = datas[i]["log_id"];
                    var log_fullname = datas[i]["log_fullname"];
                    var log_role = datas[i]["log_role"];
                    var log_in = datas[i]["log_in"];
                    var log_out = datas[i]["log_out"];
                    var date_created = datas[i]["date_created"];

                    tmpl += "<tr>"+
                            "<td>"+ log_id +"</td>"+
                            "<td>"+ log_fullname +"</td>"+
                            "<td>"+ log_role +"</td>"+
                            "<td>"+ log_in +"</td>"+
                            "<td>"+ log_out +"</td>"+
                            "</tr>";
                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-logs").find("tbody tr").remove().end();
            $("#tbl-logs").append(tmpl);
            $('#tbl-logs').DataTable();
            
        }


    });

</script>