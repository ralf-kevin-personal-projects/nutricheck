<?php include '../headers/dashboard-header.php'; ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Feeding Site</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Data Entry</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Feeding Site</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="ecommerce-widget">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <!-- <label>Date: </label>
                                        <input type="date" class="form-control" /> -->
                                    </div>
                                    <div class="col-lg-5">
                                        <!-- <label>Parish: </label>
                                        <select class="form-control">
                                            <option>SELECT PARISH</option>
                                        </select> -->
                                    </div>
                                    <div class="col-md-2">
                                        <!-- <label>&emsp;</label> -->
                                        <button id="btn-add" class="form-control btn btn-md btn-primary">Add New Site</button>
                                    </div>                                    

                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table id="tbl-fs" class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">Site No.</th>
                                                    <th class="border-0">Name of Site</th>
                                                    <th class="border-0">Name of Parish</th>
                                                    <th class="border-0">Supervised by</th>
                                                    <th class="border-0">Status</th>
                                                    <th class="border-0">Date Started</th>
                                                    <th class="border-0">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- <tr>
                                                    <td>
                                                        <div class="m-r-10"><img src="assets/images/product-pic.jpg" alt="user" class="rounded" width="45"></div>
                                                    </td>
                                                    <td>Product #1 </td>
                                                    <td>id000001 </td>
                                                    <td>id000001 </td>
                                                    <td>id000001 </td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <button id="btn-edit" class="btn btn-success btn-sm"><i class="fas fa-edit"></i></button>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button id="btn-delete" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                            </div>                                                            
                                                        </div>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>                  
                </div>



            </div>

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        Copyright © 2018 Nutricheck. All rights reserved.
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->


<!-- Modal -->
<div class="modal fade" id="fsModal" tabindex="-1" role="dialog" aria-labelledby="fsModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fsModalTitle">Add New Company Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group">
            
                <h6>Site Information</h6>
                <div class="row">
                    <div class="col-md-12">
                        <label>Site Name</label>                                                                
                        <div class="form-group">
                            <input id="site_name" type="text" class="form-control" placeholder="Site Name"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Parish</label>                                                                
                        <div class="form-group">
                            <input id="parish_name" type="text" class="form-control" placeholder="Parish"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Coordinator</label>                                                                
                        <div class="form-group">
                            <select id="coord_list" class="form-control">
                                <option value="">SELECT COORDINATOR</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Date Started</label>                                                                
                        <div class="form-group">
                            <input id="date_started" type="date" class="form-control" placeholder=""/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Status</label>                                                                
                        <div class="form-group">
                            <select id="status" class="form-control">
                                <option value="">SELECT STATUS</option>
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>
                        </div>
                    </div>                    
                </div>

            </div>
            <hr/>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick=location.reload()>Close</button>
        <button id="btnSave" type="button" class="btn btn-success">Create Company Account</button>
      </div>
    </div>
  </div>
</div>


<?php include '../headers/dashboard-footer.php'; ?>

<script>

    $(document).ready(function(){

        loadCoordList();
        loadData();
        init();
        
        var date_finish;

        $("#site_name").change(function(){
            if (!isValid(this.value)) {
                alert("site name is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#parish_name").change(function(){
            if (!isValid(this.value)) {
                alert("parish name is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });              


        $(document).on("keyup", "#site_name", function(){
            var site_name = $("#site_name").val();
            var hide = false;

            $("#tbl-fs").find("tr").each(function(){

                var checkItem = $(this).find("td:eq(1)").text();

                if (checkItem != null && checkItem != "") {

                    if (site_name == checkItem) {
                        alert("Sitename already taken")
                        $("#site_name").addClass("border-danger");
                        hide = true;
                    }
                }

            });

            if (hide) {
                $("#btnSave").hide();
            } else {
                $("#site_name").removeClass("border-danger");                                
                $("#btnSave").show();
            }


        });


        $(document).on("click","#btn-delete", function(){
            var id = $(this).data("id");
            // alert("id " + id)

            fsRequest(id, "del_fs");
        });

        $(document).on("click","#btn-edit", function(){

            var fs_id = $(this).data("id");
            var site = $(this).data("site");
            var parish = $(this).data("parish");
            var full = $(this).data("full");
            var status = $(this).data("status");
            var date_started = $(this).data("date");

            localStorage.setItem("fs_id", fs_id);

            $("#site_name").val(site);
            $("#parish_name").val(parish);
            $("#coord_list").val(full);
            $("#date_started").val(date_started);
            $("#status").val(status);

            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#fsModalTitle").text("View Feeding Site");
            $("#fsModal").modal("show");
        });

        $(document).on("click","#btn-view", function(){

            var fs_id = $(this).data("id");
            var site = $(this).data("site");
            var parish = $(this).data("parish");
            var full = $(this).data("full");
            var status = $(this).data("status");
            var date_started = $(this).data("date");

            $("#site_name").val(site).attr("disabled", true);
            $("#parish_name").val(parish).attr("disabled", true);
            $("#coord_list").val(full).attr("disabled", true);
            $("#date_started").val(date_started).attr("disabled", true);
            $("#status").val(status).attr("disabled", true);

            $("#btnSave").hide();
            $("#fsModalTitle").text("Update Feeding Site");
            $("#fsModal").modal("show");
        });

        $(document).on("click","#btn-add", function(){
            $("#btnSave").text("Add New Site");
            $("#btnSave").show();
            $("#fsModalTitle").text("Add New Site");
            $("#fsModal").modal("show");
        });    

        $("#btnSave").click(function(){
            var cmd = $(this).text();

            var coord_id = $("#coord_list").find("option:selected").data("id");
            localStorage.setItem("coord_id", coord_id);

            var site_key = "#site_name";
            var parish_key = "#parish_name";
            var coord_key = "#coord_list";
            var date_started_key = "#date_started";
            var status_key = "#status";

            var site = $(site_key).val();
            var parish = $(parish_key).val();
            var coord = $(coord_key).val();
            var date_started = $(date_started_key).val();
            var status = $(status_key).val();

            var values = [site, parish, coord, date_started, status];
            var keys = [site_key, parish_key, coord_key, date_started_key, status_key];

            if (validateItems(values, keys)) {
                switch(cmd) {
                    case "Add New Site":
                        fsRequest(values, "create_fs");
                    break;
                    case "Save Changes":
                        fsRequest(values, "update_fs");
                    break;
                }

            } else {
                alert("Please input all the empty fields");
            }

        });


        function fsRequest(values, request) {

            var fd = new FormData();

            if (request != "del_fs") {

                switch(request) {
                    case "create_fs":
                        fd.append("request", "create_fs");
                    break;
                    case "update_fs":
                        fd.append("id", localStorage.getItem("fs_id"));
                        fd.append("request", "update_fs");
                    break;
                }

                fd.append("site", values[0]);
                fd.append("parish", values[1]);
                fd.append("coord", values[2]);
                fd.append("date_started", values[3]);
                fd.append("date_finish", getFinishDate(values[3]));
                fd.append("status", values[4]);
                fd.append("coord_id", localStorage.getItem("coord_id"));

            } else {
                fd.append("id", values);
                fd.append("request", "del_fs");                
            }


            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    alert(res.result);
                    
                    if(request == "create_fs") {
                        clearItems();
                    }

                    loadData();
                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
            
        }


        function loadData() {

            var fd = new FormData();
            fd.append("request", "fetch_fs");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }


        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var fs_id = datas[i]["fs_id"];
                    var fs_site = datas[i]["fs_site"];
                    var fs_parish = datas[i]["fs_parish"];
                    var coord_fullname = datas[i]["coord_fullname"];
                    var fs_status = datas[i]["fs_status"];
                    var fs_date_started = datas[i]["fs_date_started"];

                    tmpl += "<tr>"+
                            "<td>"+ fs_id +"</td>"+
                            "<td>"+ fs_site +"</td>"+
                            "<td>"+ fs_parish +"</td>"+
                            "<td>"+ coord_fullname +"</td>"+
                            "<td>"+ fs_status +"</td>"+
                            "<td>"+ fs_date_started +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-edit' class='btn btn-sm btn-success' "+
                                        "data-id='"+ fs_id +"' "+
                                        "data-site='"+ fs_site +"' "+
                                        "data-parish='"+ fs_parish +"' "+
                                        "data-full='"+ coord_fullname +"' "+
                                        "data-status='"+ fs_status +"' "+
                                        "data-date='"+ fs_date_started +"' >"+                              
                                        "<i class='fas fa-edit'></i>"+
                                    "</button> "+
                                    "<button id='btn-view' class='btn btn-sm btn-primary' "+
                                        "data-id='"+ fs_id +"' "+
                                        "data-site='"+ fs_site +"' "+
                                        "data-parish='"+ fs_parish +"' "+
                                        "data-full='"+ coord_fullname +"' "+
                                        "data-status='"+ fs_status +"' "+
                                        "data-date='"+ fs_date_started +"' >"+
                                        "<i class='fas fa-eye'></i>"+
                                    "</button> "+                    
                                    "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                        "data-id='"+ fs_id +"' >"+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button> "+
                                "</div>"+
                            "</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-fs").find("tbody tr").remove().end();
            $("#tbl-fs").append(tmpl);
            $('#tbl-fs').DataTable();
            
        }


        function loadCoordList() {
            
            var fd = new FormData();
            fd.append("request", "fetch_coord");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateCLData(res.result);
                    } else {
                        alert(res.result);
                    }

                }, error: function() {
                    alert("error handler")
                }
            });        
        }

        function populateCLData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var id = datas[i]["coord_id"];
                    var fullname = datas[i]["coord_fullname"];

                    tmpl += "<option value="+ fullname +" data-id="+ id +">"+ fullname +"</option>";

                }
                
            } else {
                Alert("No Result");
                $("#coord_list").find("option").remove().end();
            }

            // $("#tbl-coord").find("tbody tr").remove().end();
            $("#coord_list").append(tmpl);
            
        }

        function getFinishDate(date) {
            var dateSplit = date.split("-");

            var year = dateSplit[0];
            var month = parseInt(dateSplit[1], 10) + 6;
            var date = dateSplit[2];

            var date_finish = year + "-" + month + "-" + date;

            return date_finish;
        }

        function validateItems(values, keys) {

            var isNotEmpty = false;
            var cTrue = 0;
            var cFalse = 0;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");

                    cFalse++;
                } else {

                    cTrue++;
                }
            }

            if (cTrue > cFalse) {
                isNotEmpty = true;
            } else {
                isNotEmpty = false;
            }

            return isNotEmpty;

        }

        function clearItems() {

            var site_key = "#site_name";
            var parish_key = "#parish_name";
            var coord_key = "#coord_list";
            var date_started_key = "#date_started";
            var status_key = "#status";

            $(site_key).val("");
            $(parish_key).val("");
            $(coord_key).val("");
            $(date_started_key).val("");
            $(status_key).val("");
        }

        function init() {

            Date.prototype.toDateInputValue = (function(){
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });

            $("#date_started").val(new Date().toDateInputValue());
        }

        function isValid(param) {
            var strReg = new RegExp("^[A-Za-z0-9]+$");
            
            if (strReg.test(param)) {
                return true;
            } else {
                return false;
            }
        }

    });

</script>