<?php include '../headers/dashboard-header.php'; ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Beneficiary</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Data Entry</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Beneficiary</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="ecommerce-widget">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <!-- <label>Date: </label>
                                        <input type="date" class="form-control" /> -->
                                    </div>
                                    <div class="col-lg-5">
                                        <!-- <label>Parish: </label>
                                        <select class="form-control">
                                            <option>SELECT PARISH</option>
                                        </select> -->
                                    </div>
                                    <div class="col-md-2">
                                        <!-- <label>&emsp;</label> -->
                                        <button id="btn-add" class="form-control btn btn-md btn-primary">Add Beneficiary</button>
                                    </div>                                    

                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table id="tbl-b" class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">Beneficiary No.</th>
                                                    <th class="border-0">Firstname</th>
                                                    <th class="border-0">Middlename</th>
                                                    <th class="border-0">Lastname</th>
                                                    <th class="border-0">Name of Parish</th>
                                                    <th class="border-0">Sex</th>                                                    
                                                    <th class="border-0">Height (Current)</th>
                                                    <th class="border-0">Weight (Current)</th>
                                                    <th class="border-0">BMI Status (Current)</th>
                                                    <th class="border-0">No. of Attended days</th>
                                                    <th class="border-0">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>                  
                </div>



            </div>

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        Copyright © 2018 Nutricheck. All rights reserved.
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->


<!-- Modal -->
<div class="modal fade" id="bModal" tabindex="-1" role="dialog" aria-labelledby="bModalTitle" aria-hidden="true">
  <div id="modal-dialog" class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="bModalTitle">Add Beneficiary</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group bene">
            
                <h6>Beneficiary Information</h6>
                <div class="row">
                    <div class="col-md-12">
                        <label>First Name</label>                                                                
                        <div class="form-group">
                            <input id="fname" type="text" class="form-control" placeholder="First Name"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Middle Name</label>                                                                
                        <div class="form-group">
                            <input id="mname" type="text" class="form-control" placeholder="Middle Name"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Last Name</label>                                                                
                        <div class="form-group">
                            <input id="lname" type="text" class="form-control" placeholder="Last Name"/>
                        </div>
                    </div>                    
                    <div class="col-md-12">
                        <label>Date of Birth</label>                                                                
                        <div class="form-group">
                            <input id="dob" type="date" class="form-control" placeholder="Date of Birth"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Parish</label>                                                                
                        <div class="form-group">
                            <select id="parish" class="form-control">
                                <option value="">SELECT PARISH</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Address</label>                                                                
                        <div class="form-group">
                            <input id="address" type="text" class="form-control" placeholder="Address"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Sex</label>                                                                
                        <div class="form-group">
                            <select id="gender" class="form-control">
                                <option value="">SELECT SEX</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>                    
                    <div class="col-md-12">
                        <label>Height (cm)</label>                                                                
                        <div class="form-group">
                            <input id="height" type="number" class="form-control" placeholder="Height (cm)"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Weight (Kg)</label>                                                                
                        <div class="form-group">
                            <input id="weight" type="number" class="form-control" placeholder="Weight (Kg)"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>BMI Status</label>                                                                
                        <div class="form-group">
                            <select id="bmi" class="form-control">
                                <option value="">SELECT STATUS</option>
                                <option value="Underweight">Underweight</option>
                                <option value="Normal">Normal</option>
                                <option value="Overweight">Overweight</option>
                                <option value="Obese">Obese</option>
                            </select>
                        </div>
                    </div>                    
                    <div class="col-md-12">
                        <label>Date Started</label>                                                                
                        <div class="form-group">
                            <input id="date_started" type="date" class="form-control" placeholder=""/>
                        </div>
                    </div>               
                </div>

            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick=location.reload()>Close</button>
        <button id="btnSave" type="button" class="btn btn-success"></button>
      </div>
    </div>
  </div>
</div>


<!-- FOR BMI -->
    <div class="form-group bmi">
            
            <h6>BMI Information</h6>
            <div class="row">    

                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Height (Baseline)</h5>
                                <div class="metric-value d-inline-block">
                                    <h4 id="bmi-height" class="mb-1">150</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Weight (Baseline)</h5>
                                <div class="metric-value d-inline-block">
                                    <h4 id="bmi-weight" class="mb-1">150</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">BMI Status (Baseline)</h5>
                                <div class="metric-value d-inline-block">
                                    <h4 id="bmi-status" class="mb-1">150</h4>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Height (Current)</h5>
                                <div class="metric-value d-inline-block">
                                    <h4 id="bmi-height-new" class="mb-1">150</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Weight (Current)</h5>
                                <div class="metric-value d-inline-block">
                                    <h4 id="bmi-weight-new" class="mb-1">150</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">BMI Status (Current)</h5>
                                <div class="metric-value d-inline-block">
                                    <h4 id="bmi-status-new" class="mb-1">150</h4>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="table-responsive">
                        <table id="tbl-bmi" class="table">
                            <thead class="bg-light">
                                <tr class="border-0">
                                    <th class="border-0">Month</th>
                                    <th class="border-0">Height</th>
                                    <th class="border-0">Weight</th>
                                    <th class="border-0">BMI Status</th>
                                    <th class="border-0">Attended Days</th>
                                    <th class="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>


            </div>
            <hr/>



            <h6>Update BMI (Monthly)</h6>
            <div class="row">
                <div class="col-md-6">
                    <label>Month</label>                                                                
                    <div class="form-group">
                        <select id="update-month" class="form-control">
                            <option value="">SELECT MONTH</option>
                            <option value="January">January</option>
                            <option value="February">February</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>Attended days</label>                                                                
                    <div class="form-group">
                        <input id="update-att" type="number" class="form-control" placeholder="Attended days"/>
                    </div>
                </div>                        
                <div class="col-md-4">
                    <label>Height (Update)</label>                                                                
                    <div class="form-group">
                        <input id="update-height" type="number" class="form-control" placeholder="Height (cm)"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <label>Weight (Update)</label>                                                                
                    <div class="form-group">
                        <input id="update-weight" type="number" class="form-control" placeholder="Weight (kg)"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <label>BMI (Update)</label>                                                                
                    <div class="form-group">
                        <input id="update-status" type="text" class="form-control" placeholder="BMI" disabled/>
                    </div>
                </div>                

            </div>
    </div>




<?php include '../headers/dashboard-footer.php'; ?>

<script>

    $(document).ready(function(){

        loadFSList();
        loadData();
        init();

        $("#fname").change(function(){
            if (!isValid(this.value)) {
                alert("firstname is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#mname").change(function(){
            if (!isValid(this.value)) {
                alert("middle name is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#lname").change(function(){
            if (!isValid(this.value)) {
                alert("last name is invalid, please check your format");
                this.value = "";                
                this.focus();
            }
        });

        $("#height").change(function(){
            if (!isValidNumber(this.value)) {
                alert("height is invalid, please check your format");
                this.value = "";
                this.focus();
            }
        });

        $("#weight").change(function(){
            if (!isValidNumber(this.value)) {
                alert("weight is invalid, please check your format");
                this.value = "";
                this.focus();
            }
        });        

        $(document).on("keyup", "#weight", function(){
            var height = parseFloat($("#height").val());
            var weight = parseFloat($(this).val());

            var bmi = weight / Math.pow(height/100, 2);

            var bmiStatus = getBMIStatus(bmi);

            $("#bmi").val(bmiStatus);

            if ($(this).val() > 0) {
                if (bmiStatus != "Underweight") {
                    alert("This beneficiary does not quality, he's already healthy!")
                    $("#btnSave").hide();
                }
            } else {
                $("#btnSave").show();
            }


        });

        $(document).on("keyup", "#update-weight", function(){
            var height = parseFloat($("#update-height").val());
            var weight = parseFloat($(this).val());

            var bmi = weight / Math.pow(height/100, 2);

            var bmiStatus = getBMIStatus(bmi);
            $("#update-status").val(bmiStatus);

            if ($(this).val() > 0) {

                if (bmiStatus != "Underweight") {
                    alert("Congratulation! another success beneficiary")
                }

            }


        });


        $(document).on("click","#btn-delete", function(){
            var id = $(this).data("id");

            bRequest(id, "del_b")
        });


        $(document).on("click","#btn-edit", function(){

            var b_id = $(this).data("id");
            var b_fname = $(this).data("fname");
            var b_mname = $(this).data("mname");
            var b_lname = $(this).data("lname");
            var b_dob = $(this).data("dob");
            var b_parish = $(this).data("parish");
            var b_address = $(this).data("add");
            var b_height = $(this).data("height");
            var b_weight = $(this).data("weight");
            var b_bmi_status = $(this).data("bmi");
            var b_gender = $(this).data("gender");
            var b_height_new = $(this).data("height-new");
            var b_weight_new = $(this).data("weight-new");
            var b_bmi_status_new = $(this).data("bmi-new");
            var b_date_started = $(this).data("started");
            var b_date_finish = $(this).data("finish");
            var fs_id = $(this).data("fs-id");

            localStorage.setItem("b_height", b_height);

            var fname_key = "#fname";
            var mname_key = "#mname";
            var lname_key = "#lname";
            var dob_key = "#dob";
            var parish_key = "#parish";
            var add_key = "#address";
            var height_key = "#height";
            var weight_key = "#weight";
            var gender_key = "#gender";
            var bmi_key = "#bmi";
            var date_started_key = "#date_started";

            $(fname_key).val(b_fname);
            $(mname_key).val(b_mname);
            $(lname_key).val(b_lname);
            $(dob_key).val(b_dob);
            $(parish_key).val(b_parish);
            $(add_key).val(b_address);
            $(height_key).val(b_height);
            $(weight_key).val(b_weight);
            $(gender_key).val(b_gender);
            $(bmi_key).val(b_bmi_status);
            $(date_started_key).val(b_date_started);

            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#bModalTitle").text("Edit Beneficiary");
            $("#bModal").modal("show");
        });

        $(document).on("click","#btn-view", function(){

            var b_id = $(this).data("id");
            var b_fname = $(this).data("fname");
            var b_mname = $(this).data("mname");
            var b_lname = $(this).data("lname");
            var b_dob = $(this).data("dob");
            var b_parish = $(this).data("parish");
            var b_address = $(this).data("add");
            var b_height = $(this).data("height");
            var b_weight = $(this).data("weight");
            var b_bmi_status = $(this).data("bmi");
            var b_gender = $(this).data("gender");
            var b_height_new = $(this).data("height-new");
            var b_weight_new = $(this).data("weight-new");
            var b_bmi_status_new = $(this).data("bmi-new");
            var b_date_started = $(this).data("started");
            var b_date_finish = $(this).data("finish");
            var fs_id = $(this).data("fs-id");

            var fname_key = "#fname";
            var mname_key = "#mname";
            var lname_key = "#lname";
            var dob_key = "#dob";
            var parish_key = "#parish";
            var add_key = "#address";
            var height_key = "#height";
            var weight_key = "#weight";
            var gender_key = "#gender";
            var bmi_key = "#bmi";
            var date_started_key = "#date_started";

            $(fname_key).val(b_fname).attr("disabled", true);
            $(mname_key).val(b_mname).attr("disabled", true);
            $(lname_key).val(b_lname).attr("disabled", true);
            $(dob_key).val(b_dob).attr("disabled", true);
            $(parish_key).val(b_parish).attr("disabled", true);
            $(add_key).val(b_address).attr("disabled", true);
            $(height_key).val(b_height).attr("disabled", true);
            $(weight_key).val(b_weight).attr("disabled", true);
            $(gender_key).val(b_gender).attr("disabled", true);
            $(bmi_key).val(b_bmi_status).attr("disabled", true);
            $(date_started_key).val(b_date_started).attr("disabled", true);


            $("#btnSave").hide();
            $("#bModalTitle").text("Update Beneficiary");
            $("#bModal").modal("show");
        });

        $(document).on("click", "#btn-bmi", function(){

            var fullname = $(this).data("fullname");
            var b_height = $(this).data("height");
            var b_weight = $(this).data("weight");
            var b_bmi_status = $(this).data("bmi");
            var b_height_new = $(this).data("height-new");
            var b_weight_new = $(this).data("weight-new");
            var b_bmi_status_new = $(this).data("bmi-new");
            var b_attended_days = $(this).data("bmi-att");
            var b_id = $(this).data("id");

            localStorage.setItem("b_id", b_id);
            localStorage.setItem("b_attended_days", b_attended_days);
            localStorage.setItem("b_height_new", b_height_new);

            var bmi = $(".bmi").html();

            if (b_bmi_status_new != "Underweight") {
                $("#btnSave").hide();
                $(".modal-body")
                    .append("<div class='alert alert-success' role='alert'>"+
                              "Congrats! BMI of beneficiary is in Normal Status.</div>");

                loadDataBMI(b_id, true);                              
            } else {
                $("#btnSave").show();
                loadDataBMI(b_id, false);
            }


            $(".modal-body").find(".bene").remove().end();
            $(".modal-body").append(bmi);

            $("#bmi-height").text(b_height);
            $("#bmi-weight").text(b_weight);
            $("#bmi-status").text(b_bmi_status);
            $("#bmi-height-new").text(b_height_new);
            $("#bmi-weight-new").text(b_weight_new);
            $("#bmi-status-new").text(b_bmi_status_new);

            $("#btnSave").text("Update BMI");
            $("#bModalTitle").text("BMI Status - " + fullname);

            $("#modal-dialog").removeClass("modal-md");
            $("#modal-dialog").addClass("modal-lg");
            $("#bModal").modal("show");



        });

        $(document).on("click", "#btn-bmi-edit", function(){

            var bmi_id = $(this).data("id");
            var bmi_month = $(this).data("month");
            var bmi_height = $(this).data("height");
            var bmi_weight = $(this).data("weight");
            var bmi_status = $(this).data("status");
            var att = $(this).data("att");

            localStorage.setItem("bmi_id", bmi_id);

            var month_key = "#update-month";
            var height_key = "#update-height";
            var weight_key = "#update-weight";
            var status_key = "#update-status";
            var att_key = "#update-att";

            $(month_key).val(bmi_month);
            $(height_key).val(bmi_height);
            $(weight_key).val(bmi_weight);
            $(status_key).val(bmi_status);
            $(att_key).val(att);

            $("#btnSave").text("Update BMI *");
            $("#btnSave").show();
        });


        $(document).on("click","#btn-add", function(){
            $("#btnSave").text("Add Beneficiary");
            $("#btnSave").show();
            $("#bModalTitle").text("Add Beneficiary");
            $("#bModal").modal("show");
        });    


        $("#btnSave").click(function(){
            var cmd = $(this).text();

            if (cmd != "Update BMI" && cmd != "Update BMI *") {

                var fs_id = $("#parish").find("option:selected").data("id");
                localStorage.setItem("fs_id", fs_id);

                var fname_key = "#fname";
                var mname_key = "#mname";
                var lname_key = "#lname";
                var dob_key = "#dob";
                var parish_key = "#parish";
                var add_key = "#address";
                var height_key = "#height";
                var weight_key = "#weight";
                var gender_key = "#gender";
                var bmi_key = "#bmi";
                var date_started_key = "#date_started";

                var fname = $(fname_key).val();
                var mname = $(mname_key).val();
                var lname = $(lname_key).val();
                var dob = $(dob_key).val();
                var parish = $(parish_key).val();
                var add = $(add_key).val();
                var height = $(height_key).val();
                var weight = $(weight_key).val();
                var gender = $(gender_key).val();
                var bmi = $(bmi_key).val();
                var date_started = $(date_started_key).val();

                var values = [fname, mname, lname, dob, parish, add, height, weight, gender, bmi, date_started];
                var keys = [fname_key, mname_key, lname_key, dob_key, parish_key, add_key, height_key, weight_key, 
                            gender_key, bmi_key, date_started_key];

                if (validateItems(values, keys)) {
                    switch(cmd) {
                        case "Add Beneficiary":
                            if (beneficiary()){
                                bRequest(values, "create_b");                            
                            }
                        break;
                        case "Save Changes":
                            var currHeight = localStorage.getItem("b_height");
                            if (currHeight > height) {
                                alert("Invalid height, please check your input")
                            } else {
                                bRequest(values, "update_b");
                            }

                        break;
                    }

                } else {
                    alert("Please input all the empty fields");
                }


            } else {

                var month_key = "#update-month";
                var height_key = "#update-height";
                var weight_key = "#update-weight";
                var status_key = "#update-status";
                var att_key = "#update-att";

                var month = $(month_key).val();
                var height = $(height_key).val();
                var weight = $(weight_key).val();
                var status = $(status_key).val();
                var att = $(att_key).val();

                var values = [month, height, weight, status, att];
                var keys = [month_key, height_key, weight_key, status_key, att_key];

                if (validateItems(values, keys)) {
                    var currHeight = localStorage.getItem("b_height_new");
                    if (cmd == "Update BMI *") {
                        if (currHeight > height) {
                            alert("Invalid New Height, please check your input");
                        } else {
                            bRequest(values, "update_bmi_upd");
                        }
                    } else {
                        if (currHeight > height) {
                            alert("Invalid New Height, please check your input");
                        } else {
                            bRequest(values, "update_bmi");
                        }
                    }

                } else {
                    alert("Please input all the empty fields");                    
                }
            }



        });



        function init() {

            Date.prototype.toDateInputValue = (function(){
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });

            $("#date_started").val(new Date().toDateInputValue());
            $(".bmi").hide();
        }



        function bRequest(values, request) {

            var fd = new FormData();

            if (request != "update_bmi" && request != "update_bmi_upd") {

                switch(request) {
                    case "create_b":
                        fd.append("request", "create_b");

                        fd.append("fname", values[0]);
                        fd.append("mname", values[1]);
                        fd.append("lname", values[2]);
                        fd.append("dob", values[3]);
                        fd.append("parish", values[4]);
                        fd.append("address", values[5]);
                        fd.append("height", values[6]);
                        fd.append("weight", values[7]);
                        fd.append("gender", values[8]);
                        fd.append("bmi", values[9]);
                        fd.append("date_started", values[10]);
                        fd.append("date_finish", getFinishDate(values[10]));
                        fd.append("fs_id", localStorage.getItem("fs_id"));                        
                    break;
                    case "update_b":
                        fd.append("id", localStorage.getItem("b_id"));
                        fd.append("request", "update_b");

                        fd.append("fname", values[0]);
                        fd.append("mname", values[1]);
                        fd.append("lname", values[2]);
                        fd.append("dob", values[3]);
                        fd.append("parish", values[4]);
                        fd.append("address", values[5]);
                        fd.append("height", values[6]);
                        fd.append("weight", values[7]);
                        fd.append("gender", values[8]);
                        fd.append("bmi", values[9]);
                        fd.append("date_started", values[10]);
                        fd.append("date_finish", getFinishDate(values[10]));
                        fd.append("fs_id", localStorage.getItem("fs_id"));                        
                    break;
                    case "del_b":
                        fd.append("id", values);
                        fd.append("request", "del_b");
                    break;
                }

            } else {
                
                switch (request) {
                    case "update_bmi":
                        fd.append("id", localStorage.getItem("b_id"));
                        fd.append("request", "update_bmi");
                    break;
                    case "update_bmi_upd":
                        fd.append("id", localStorage.getItem("b_id"));
                        fd.append("bmi_id", localStorage.getItem("bmi_id"));
                        fd.append("request", "update_bmi_upd");
                    break;
                }

                var totalAtt = parseInt(values[4]) + parseInt(localStorage.getItem("b_attended_days"));

                fd.append("month", values[0]);
                fd.append("height", values[1]);
                fd.append("weight", values[2]);
                fd.append("bmi", values[3]);
                fd.append("att", values[4]);
                fd.append("total_att", totalAtt);
            }

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    alert(res.result);

                    if (request == "update_bmi" && request == "update_bmi_upd"){
                        loadDataBMI(localStorage.getItem("b_id"), false);
                    }


                    loadData();
                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }


        function loadData() {

            var fd = new FormData();
            fd.append("fs_id", localStorage.getItem("fs_id"));
            fd.append("request", "fetch_b");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                }, error: function() {
                    alert("error handler")
                }
            });
        }



        function loadDataBMI(id, disable) {

            var fd = new FormData();
            fd.append("id", id);
            fd.append("request", "fetch_bmi");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateDataBMI(res.result, disable);
                    } else {
                        alert(res.result);
                    }
                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }



        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var b_id = datas[i]["b_id"];
                    var b_fname = datas[i]["b_fname"];
                    var b_mname = datas[i]["b_mname"];
                    var b_lname = datas[i]["b_lname"];
                    var b_dob = datas[i]["b_dob"];
                    var b_parish = datas[i]["b_parish"];
                    var b_address = datas[i]["b_address"];
                    var b_height = datas[i]["b_height"];
                    var b_weight = datas[i]["b_weight"];
                    var b_bmi_status = datas[i]["b_bmi_status"];
                    var b_gender = datas[i]["b_gender"];
                    var b_height_new = datas[i]["b_height_new"];
                    var b_weight_new = datas[i]["b_weight_new"];
                    var b_bmi_status_new = datas[i]["b_bmi_status_new"];
                    var b_date_started = datas[i]["b_date_started"];
                    var b_date_finish = datas[i]["b_date_finish"];
                    var b_attended_days = datas[i]["b_attended_days"];                    
                    var fs_id = datas[i]["fs_id"];
                    
                    var fullname = b_fname + " " + b_mname +" " + b_lname;

                    tmpl += "<tr>"+
                            "<td>"+ b_id +"</td>"+
                            "<td>"+ b_fname +"</td>"+
                            "<td>"+ b_mname +"</td>"+
                            "<td>"+ b_lname +"</td>"+
                            "<td>"+ b_parish +"</td>"+
                            "<td>"+ b_gender +"</td>"+                            
                            "<td>"+ b_height_new +"</td>"+
                            "<td>"+ b_weight_new +"</td>"+
                            "<td>"+ b_bmi_status_new +"</td>"+
                            "<td>"+ b_attended_days +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-edit' class='btn btn-sm btn-success' "+
                                        "data-id='"+ b_id +"' "+
                                        "data-fname='"+ b_fname +"' "+
                                        "data-mname='"+ b_mname +"' "+
                                        "data-lname='"+ b_lname +"' "+
                                        "data-dob='"+ b_dob +"' "+
                                        "data-parish='"+ b_parish +"' "+
                                        "data-add='"+ b_address +"' "+
                                        "data-height='"+ b_height +"' "+
                                        "data-weight='"+ b_weight +"' "+
                                        "data-bmi='"+ b_bmi_status +"' "+
                                        "data-gender='"+ b_gender +"' "+
                                        "data-height-new='"+ b_height_new +"' "+
                                        "data-weight-new='"+ b_weight_new +"' "+
                                        "data-bmi-new='"+ b_bmi_status_new +"' "+
                                        "data-started='"+ b_date_started +"' "+
                                        "data-finish='"+ b_date_finish +"' "+
                                        "data-fs-id='"+ fs_id +"' >"+                              
                                        "<i class='fas fa-edit'></i>"+
                                    "</button> "+
                                    "<button id='btn-view' class='btn btn-sm btn-primary' "+
                                        "data-id='"+ b_id +"' "+
                                        "data-fname='"+ b_fname +"' "+
                                        "data-mname='"+ b_mname +"' "+
                                        "data-lname='"+ b_lname +"' "+
                                        "data-dob='"+ b_dob +"' "+
                                        "data-parish='"+ b_parish +"' "+
                                        "data-add='"+ b_address +"' "+
                                        "data-height='"+ b_height +"' "+
                                        "data-weight='"+ b_weight +"' "+
                                        "data-bmi='"+ b_bmi_status +"' "+
                                        "data-gender='"+ b_gender +"' "+                                        
                                        "data-height-new='"+ b_height_new +"' "+
                                        "data-weight-new='"+ b_weight_new +"' "+
                                        "data-bmi-new='"+ b_bmi_status_new +"' "+
                                        "data-started='"+ b_date_started +"' "+
                                        "data-finish='"+ b_date_finish +"' "+
                                        "data-fs-id='"+ fs_id +"' >"+                              
                                        "<i class='fas fa-eye'></i>"+
                                    "</button> "+
                                    "<button id='btn-bmi' class='btn btn-sm btn-warning' "+
                                        "data-fullname='"+ fullname +"' "+
                                        "data-height='"+ b_height +"' "+
                                        "data-weight='"+ b_weight +"' "+
                                        "data-bmi='"+ b_bmi_status +"' "+
                                        "data-height-new='"+ b_height_new +"' "+
                                        "data-weight-new='"+ b_weight_new +"' "+
                                        "data-bmi-new='"+ b_bmi_status_new +"' "+
                                        "data-bmi-att='"+ b_attended_days +"' "+
                                        "data-id='"+ b_id +"' >"+                                                                                
                                        "<i class='fas fa-list'></i>"+
                                    "</button> "+
                                    "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                        "data-id='"+ b_id +"' >"+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button> "+
                                "</div>"+
                            "</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-b").find("tbody tr").remove().end();
            $("#tbl-b").append(tmpl);
            $('#tbl-b').DataTable();
            
        }



        function populateDataBMI(datas, disable) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var b_id = datas[i]["b_id"];
                    var bmi_id = datas[i]["bmi_id"];
                    var bmi_month = datas[i]["bmi_month"];
                    var bmi_height = datas[i]["bmi_height"];
                    var bmi_weight = datas[i]["bmi_weight"];                    
                    var bmi_status = datas[i]["bmi_status"];	
                    var bmi_attendance = datas[i]["bmi_attendance"];	
                    var date_created = datas[i]["date_created"];


                    tmpl += "<tr>"+
                            "<td>"+ bmi_month +"</td>"+
                            "<td>"+ bmi_height +"</td>"+
                            "<td>"+ bmi_weight +"</td>"+
                            "<td>"+ bmi_status +"</td>"+
                            "<td>"+ bmi_attendance +"</td>";

                    // if (!disable) {
                    //     tmpl += "<td>"+
                    //             "<div class='form-group'>"+
                    //                 "<button id='btn-bmi-edit' class='btn btn-xs btn-success' "+
                    //                     "data-id='"+ bmi_id +"' "+
                    //                     "data-month='"+ bmi_month +"' "+
                    //                     "data-height='"+ bmi_height +"' "+
                    //                     "data-weight='"+ bmi_weight +"' "+
                    //                     "data-status='"+ bmi_status +"' "+
                    //                     "data-att='"+ bmi_attendance +"' >"+
                    //                     "<i class='fas fa-edit'></i>"+
                    //                 "</button> "+
                    //             "</div>"+
                    //             "<div class='form-group'>"+
                    //                 "<button id='btn-bmi-del' class='btn btn-xs btn-danger' "+
                    //                     "data-id='"+ bmi_id +"' "+
                    //                     "data-month='"+ bmi_month +"' "+
                    //                     "data-height='"+ bmi_height +"' "+
                    //                     "data-weight='"+ bmi_weight +"' "+
                    //                     "data-status='"+ bmi_status +"' "+
                    //                     "data-att='"+ bmi_attendance +"' >"+
                    //                     "<i class='fas fa-trash'></i>"+
                    //                 "</button> "+
                    //             "</div>"+                                
                    //         "</td>";
                    // } else {

                        tmpl += "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-bmi-edit' class='btn btn-xs btn-success' "+
                                        "data-id='"+ bmi_id +"' "+
                                        "data-month='"+ bmi_month +"' "+
                                        "data-height='"+ bmi_height +"' "+
                                        "data-weight='"+ bmi_weight +"' "+
                                        "data-status='"+ bmi_status +"' "+
                                        "data-att='"+ bmi_attendance +"'>"+
                                        "<i class='fas fa-edit' ></i>"+
                                    "</button> "+
                                "</div>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-bmi-del' class='btn btn-xs btn-danger' "+
                                        "data-id='"+ bmi_id +"' "+
                                        "data-month='"+ bmi_month +"' "+
                                        "data-height='"+ bmi_height +"' "+
                                        "data-weight='"+ bmi_weight +"' "+
                                        "data-status='"+ bmi_status +"' "+
                                        "data-att='"+ bmi_attendance +"' >"+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button> "+
                                "</div>"+                                                                
                            "</td>";

                    // }

                    tmpl += "</tr>";


                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-bmi").find("tbody tr").remove().end();
            $("#tbl-bmi").append(tmpl);
            $('#tbl-bmi').DataTable();
            
        }



        function loadFSList() {
            
            var fd = new FormData();
            fd.append("request", "fetch_fs");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateFSData(res.result);
                    } else {
                        alert(res.result);
                    }

                }, error: function() {
                    alert("error handler")
                }

            });        
        }



        function populateFSData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var fs_id = datas[i]["fs_id"];
                    var fs_parish = datas[i]["fs_parish"];

                    tmpl += "<option value="+ fs_parish +" data-id="+ fs_id +">"+ fs_parish +"</option>";

                }
                
            } else {
                Alert("No Result");
                $("#parish").find("option").remove().end();
            }

            $("#parish").append(tmpl);
            
        }



        function getBMIStatus(bmi) {

            var status;
            if (bmi < 18.5) {
                status = "Underweight";
            } else if (bmi > 18.5 && bmi < 25) {
                status = "Normal";
            } else if (bmi > 25 && bmi < 30) {
                status = "Overweight";
            } else if (bmi > 30){
                status = "Obese";
            }

            return status;
        }



        function getFinishDate(date) {
            var dateSplit = date.split("-");

            var year = dateSplit[0];
            var month = parseInt(dateSplit[1], 10) + 6;
            var date = dateSplit[2];

            var date_finish = year + "-" + month + "-" + date;

            return date_finish;
        }



        function validateItems(values, keys) {

            var isNotEmpty = false;
            var cTrue = 0;
            var cFalse = 0;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");

                    cFalse++;
                } else {

                    cTrue++;
                }
            }

            if (cTrue > cFalse) {
                isNotEmpty = true;
            } else {
                isNotEmpty = false;
            }

            return isNotEmpty;

        }


        function beneficiary() {

            var fname = $("#fname").val();
            var mname = $("#mname").val();
            var lname = $("#lname").val();
            var hide = false;
            var ret;

            $("#tbl-b").find("tr").each(function(){

                var checkFname = $(this).find("td:eq(1)").text();
                var checkMname = $(this).find("td:eq(2)").text();
                var checkLname = $(this).find("td:eq(3)").text();

                if (checkFname != null && checkFname != "" &&
                    checkMname != null && checkMname != "" &&
                    checkLname != null && checkLname != "") {

                    if (fname == checkFname && mname == checkMname && lname == checkLname) {
                        alert("This beneficiary already exist!")
                        $("#fname").addClass("border-danger");
                        $("#mname").addClass("border-danger");
                        $("#lname").addClass("border-danger");
                        hide = true;
                    }
                }

            });

            if (hide) {
                // $("#btnSave").hide();
                ret = false;
            } else {
                $("#fname").removeClass("border-danger");                                
                $("#mname").removeClass("border-danger");                                
                $("#lname").removeClass("border-danger");                                
                // $("#btnSave").show();
                ret = true;
            }

            return ret;
        }

        // to be continue
        function clearItems() {
            var fname_key = "#fname";
            var mname_key = "#mname";
            var lname_key = "#lname";
            var dob_key = "#dob";
            var parish_key = "#parish";
            var add_key = "#address";
            var height_key = "#height";
            var weight_key = "#weight";
            var gender_key = "#gender";
            var bmi_key = "#bmi";
            var date_started_key = "#date_started";

            var fname = $(fname_key).val();
            var mname = $(mname_key).val();
            var lname = $(lname_key).val();
            var dob = $(dob_key).val();
            var parish = $(parish_key).val();
            var add = $(add_key).val();
            var height = $(height_key).val();
            var weight = $(weight_key).val();
            var gender = $(gender_key).val();
            var bmi = $(bmi_key).val();
            var date_started = $(date_started_key).val();
        }

        function isValid(param) {
            var strReg = new RegExp("^[A-Za-z0-9]+$");
            if (strReg.test(param)) {
                return true;
            } else {
                return false;
            }
        }

        function isValidNumber(param) {
            var strReg = new RegExp("^[0-9]+$");
            console.log("param: " + param)
            if (strReg.test(param)) {
                return true;
            } else {
                return false;
            }
        }        

    });

</script>