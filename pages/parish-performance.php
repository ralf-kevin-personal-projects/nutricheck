<?php include '../headers/dashboard-header.php'; ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Parish Performance</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">PM Reports</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Parish Performance</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="ecommerce-widget">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <label>Date: </label>
                                        <input id="date_started" type="date" class="form-control" />                                                                                    
                                    </div>
                                    <div class="col-lg-5">
                                        <label>Parish: </label>
                                        <select id="parish_list" class="form-control">
                                            <option value= "">SELECT PARISH</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <label>&emsp;</label>
                                        <button id="btn_search" class="form-control btn btn-md btn-primary">SEARCH</button>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>

                <div id="result">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>Date Started:</label>
                                            <label id="lbl_date" >12.09.2018 </label>                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Feeding Completion:</label>
                                            <label id="lbl_fc">98%</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                  
                    </div>            

                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <div class="table-responsive">
                                            <table id="tbl-b" class="table">
                                                <thead class="bg-light">
                                                    <tr class="border-0">
                                                        <th class="border-0">Beneficiaries</th>
                                                        <th class="border-0">Male</th>
                                                        <th class="border-0">Female</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="m-r-10"><img src="assets/images/product-pic.jpg" alt="user" class="rounded" width="45"></div>
                                                        </td>
                                                        <td>Product #1 </td>
                                                        <td>id000001 </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                        </div>                  
                    </div>


                    <div class="row">
                        <!-- ============================================================== -->
                    
                        <!-- ============================================================== -->

                                        <!-- recent orders  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-8 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Height Status</h5>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <!-- <table class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">Feeding Sites</th>
                                                    <th class="border-0">Beneficiaries</th>
                                                    <th class="border-0">Male</th>
                                                    <th class="border-0">Female</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        <div class="m-r-10"><img src="assets/images/product-pic.jpg" alt="user" class="rounded" width="45"></div>
                                                    </td>
                                                    <td>Product #1 </td>
                                                    <td>id000001 </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>
                                                        <div class="m-r-10"><img src="assets/images/product-pic-2.jpg" alt="user" class="rounded" width="45"></div>
                                                    </td>
                                                    <td>Product #2 </td>
                                                    <td>id000002 </td>
                                            </tbody>
                                        </table> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end recent orders  -->


                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- customer acquistion  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Attendance Rate</h5>
                                <div class="card-body">


                                    <div class="bar-chart">

                                        <div class="chart clearfix">

                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">51%</span>
                                    
                                                    <div class="item-progress" data-percent="51">
                                                        <span class="title">Site 1</span>
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->
                                            
                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">89.2%</span>
                                    
                                                    <div class="item-progress" data-percent="89.2">
                                                        <span class="title">Site 2</span>
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->
                                            
                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">82%</span>
                                    
                                                    <div class="item-progress" data-percent="82">
                                                        <span class="title">Comunication</span>
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->
                                    
                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">67%</span>
                                    
                                                    <div class="item-progress" data-percent="67">
                                                        <span class="title">Persuasion</span>
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->
                                        </div>
                                        <!-- //.chart -->
                                    </div>



                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end customer acquistion  -->
                        <!-- ============================================================== -->
                    </div>


                    <div class="row">
                        <!-- ============================================================== -->
                    
                        <!-- ============================================================== -->

                                        <!-- recent orders  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-8 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Weight Status</h5>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <!-- <table class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">Feeding Sites</th>
                                                    <th class="border-0">Beneficiaries</th>
                                                    <th class="border-0">Male</th>
                                                    <th class="border-0">Female</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        <div class="m-r-10"><img src="assets/images/product-pic.jpg" alt="user" class="rounded" width="45"></div>
                                                    </td>
                                                    <td>Product #1 </td>
                                                    <td>id000001 </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>
                                                        <div class="m-r-10"><img src="assets/images/product-pic-2.jpg" alt="user" class="rounded" width="45"></div>
                                                    </td>
                                                    <td>Product #2 </td>
                                                    <td>id000002 </td>
                                            </tbody>
                                        </table> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end recent orders  -->


                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- customer acquistion  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Attendance Rate</h5>
                                <div class="card-body">


                                    <div class="bar-chart">

                                        <div class="chart clearfix">

                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">51%</span>
                                    
                                                    <div class="item-progress" data-percent="51">
                                                        <span class="title">Site 1</span>
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->
                                            
                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">89.2%</span>
                                    
                                                    <div class="item-progress" data-percent="89.2">
                                                        <span class="title">Site 2</span>
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->
                                            
                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">82%</span>
                                    
                                                    <div class="item-progress" data-percent="82">
                                                        <span class="title">Comunication</span>
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->
                                    
                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">67%</span>
                                    
                                                    <div class="item-progress" data-percent="67">
                                                        <span class="title">Persuasion</span>
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->
                                        </div>
                                        <!-- //.chart -->
                                    </div>



                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end customer acquistion  -->
                        <!-- ============================================================== -->
                    </div>
                </div>


            </div>

    </div>

</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->

<?php include '../headers/dashboard-footer.php'; ?>

<script>

$(document).ready(function(){

    loadParishList();
    init();

    $(document).on("click", "#btn_search", function(){

        var date_started_key = "#date_started";
        var parish_key = "#parish_list";

        var date_started = $(date_started_key).val();
        var parish = $(parish_key).val();

        var fs_id = $(parish_key).find("option:selected").data("id");
        localStorage.setItem("fs_id", fs_id);

        var values = [date_started, parish];
        var keys = [date_started_key, parish_key];

        if (validateItems(values, keys)) {
            parishRequest(values, "fetch_parish");
        } else {
            alert("Please input all the empty fields");
        }
        
    });


    function parishRequest(value, type) {

        var fd = new FormData();
        fd.append("id", localStorage.getItem("fs_id"));
        fd.append("date_started", value[0]);
        fd.append("request", "parish_perf");

        // alert(localStorage.getItem("fs_id"));

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    populateData(res.result);
                } else {
                    alert(res.result);
                }

                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }


    function loadParishList() {

        var fd = new FormData();
        fd.append("request", "fetch_parish");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    populateParishData(res.result);
                } else {
                    alert(res.result);
                }

                // console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }

    function populateParishData(datas) {

        var tmpl;

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                var fs_id = datas[i]["fs_id"];
                var fs_parish = datas[i]["fs_parish"];

                tmpl += "<option value="+ fs_parish +" data-id="+ fs_id +">"+ fs_parish +"</option>";

            }
            
        } else {
            Alert("No Result");
            $("#parish_list").find("option").remove().end();
        }

        $("#parish_list").append(tmpl);
        
    }



    function populateData(datas) {

        var tmpl;
        var hasValue = false;

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                if (datas[i]["fs_id"] != null) {

                    var fs_id = datas[i]["fs_id"];
                    var fs_parish = datas[i]["fs_parish"];
                    var fs_date_started = datas[i]["fs_date_started"];
                    var fs_date_finish = datas[i]["fs_date_finish"];
                    var male = datas[i]["male"];
                    var female = datas[i]["female"];
                    var count_b = datas[i]["count_b"];

                    tmpl += "<tr>"+
                            "<td>"+ count_b +"</td>"+
                            "<td>"+ male +"</td>"+
                            "<td>"+ female +"</td>"+
                            "</tr>";


                    $("#lbl_date").text(fs_date_started);
                    $("#lbl_fc").text(setFeedingCompletion(fs_date_started, fs_date_finish) + "%");

                    hasValue = true;
                } else {
                    alert("No Result");
                }



            }
            
        } else {
            alert("No Result");
        }

        if (hasValue) {
            $("#result").show();
            $("#tbl-b").find("tbody tr").remove().end();
            $("#tbl-b").append(tmpl);
            $('#tbl-b').DataTable();

            barChart();
        } else {
            $("#result").hide();
        }

        
    }


    function validateItems(values, keys) {

        var isNotEmpty = false;
        var cTrue = 0;
        var cFalse = 0;

        for (var i = 0; i < values.length; i++) {

            if (values[i] == "" || values[i] == null) {
                $(keys[i]).addClass("border-danger");

                cFalse++;
            } else {

                cTrue++;
            }
        }

        if (cTrue > cFalse) {
            isNotEmpty = true;
        } else {
            isNotEmpty = false;
        }

        return isNotEmpty;

    }

    function init() {
        $("#result").hide();
    }

    function setFeedingCompletion(start, end) {
        Date.prototype.getCurrentDate = (function(){
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });

        var c = new Date().getCurrentDate();
        var curr = new Date(c).getTime();
        var start = new Date(start).getTime();
        var end = new Date(end).getTime();
        
        var complete = ((curr - start) / (end - start) ) * 100;

        return complete.toFixed(2);
    }





    //For Charts
    barChart();
    
    $(window).resize(function(){
        barChart();
    });
    
    function barChart(){
        $('.bar-chart').find('.item-progress').each(function(){
            var itemProgress = $(this),
            itemProgressWidth = $(this).parent().width() * ($(this).data('percent') / 100);
            itemProgress.css('width', itemProgressWidth);
        });
    };
});

</script>