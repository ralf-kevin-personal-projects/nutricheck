<?php include '../headers/dashboard-header.php'; ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Graduates</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page">Graduates</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="ecommerce-widget">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="offset-lg-3 col-lg-4">
                                        <select id="site_list" class="form-control">
                                            <option value= "">SELECT SITE</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <button id="btn_search" class="form-control btn btn-md btn-primary">
                                        <i class="fas fa-search"></i> SEARCH
                                        </button>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>



                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table id="tbl-b" class="table">
                                            <thead class="bg-light">
                                                <tr class="border-0">
                                                    <th class="border-0">Beneficiary No.</th>
                                                    <th class="border-0">Firstname</th>
                                                    <th class="border-0">Middlename</th>
                                                    <th class="border-0">Lastname</th>
                                                    <th class="border-0">Name of Parish</th>
                                                    <th class="border-0">Sex</th>                                                    
                                                    <th class="border-0">Height (Current)</th>
                                                    <th class="border-0">Weight (Current)</th>
                                                    <th class="border-0">BMI Status (Current)</th>
                                                    <th class="border-0">No. of Attended days</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>                  
                </div>



            </div>

    </div>
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <!-- <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        Copyright © 2018 Nutricheck. All rights reserved.
                </div>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- end footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->


<!-- Modal -->
<div class="modal fade" id="cdModal" tabindex="-1" role="dialog" aria-labelledby="cdModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cdModalTitle">Add New Coordinator</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group">
            
                <h6>Coordinator Information</h6>
                <div class="row">
                    <div class="col-md-12">
                        <label>Full Name</label>                                                                
                        <div class="form-group">
                            <input id="fullname" type="text" class="form-control" placeholder="Full Name"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Role</label>                                                                
                        <div class="form-group">
                            <input id="role" type="text" class="form-control" placeholder="Role"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Gender</label>                                                                
                        <div class="form-group">
                            <select id="gender" class="form-control">
                                <option value="">SELECT GENDER</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Account Status</label>                                                                
                        <div class="form-group">
                            <select id="status" class="form-control">
                                <option value="">SELECT STATUS</option>
                                <option value="Activated">Activated</option>
                                <option value="Deactivated">Deactivated</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Username</label>                                                                
                        <div class="form-group">
                            <input id="user" type="text" class="form-control" placeholder="Username"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Password</label>                                                                
                        <div class="form-group">
                            <input id="pass" type="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label>Confirm Password</label>                                                                
                        <div class="form-group">
                            <input id="cpass" type="password" class="form-control" placeholder="Confirm Password"/>
                        </div>
                    </div>
                </div>

            </div>
            <hr/>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="btnSave" type="button" class="btn btn-success">Create Company Account</button>
      </div>
    </div>
  </div>
</div>


<?php include '../headers/dashboard-footer.php'; ?>

<script>

    $(document).ready(function(){

        // loadData();
        loadFSList();

        $(document).on("click", "#btn_search", function(){
            var fs_id = $("#site_list").find("option:selected").data("id");
            loadData(fs_id);
        });


        function loadData(fs_id) {
            var fd = new FormData();
            fd.append("fs_id", fs_id);
            fd.append("request", "candidates");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                        $("#tbl-b").find("tbody tr").remove().end();
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }


        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var b_id = datas[i]["b_id"];
                    var b_fname = datas[i]["b_fname"];
                    var b_mname = datas[i]["b_mname"];
                    var b_lname = datas[i]["b_lname"];
                    var b_dob = datas[i]["b_dob"];
                    var b_parish = datas[i]["b_parish"];
                    var b_address = datas[i]["b_address"];
                    var b_height = datas[i]["b_height"];
                    var b_weight = datas[i]["b_weight"];
                    var b_bmi_status = datas[i]["b_bmi_status"];
                    var b_gender = datas[i]["b_gender"];
                    var b_height_new = datas[i]["b_height_new"];
                    var b_weight_new = datas[i]["b_weight_new"];
                    var b_bmi_status_new = datas[i]["b_bmi_status_new"];
                    var b_date_started = datas[i]["b_date_started"];
                    var b_date_finish = datas[i]["b_date_finish"];
                    var b_attended_days = datas[i]["b_attended_days"];                    
                    var fs_id = datas[i]["fs_id"];
                    
                    var fullname = b_fname + " " + b_mname +" " + b_lname;

                    tmpl += "<tr>"+
                            "<td>"+ b_id +"</td>"+
                            "<td>"+ b_fname +"</td>"+
                            "<td>"+ b_mname +"</td>"+
                            "<td>"+ b_lname +"</td>"+
                            "<td>"+ b_parish +"</td>"+
                            "<td>"+ b_gender +"</td>"+                            
                            "<td>"+ b_height_new +"</td>"+
                            "<td>"+ b_weight_new +"</td>"+
                            "<td>"+ b_bmi_status_new +"</td>"+
                            "<td>"+ b_attended_days +"</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-b").find("tbody tr").remove().end();
            $("#tbl-b").append(tmpl);
            // $('#tbl-b').DataTable();

            if (!$.fn.DataTable.isDataTable('#tbl-b') ) {
                $('#tbl-b').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                });
            }
        }


        function loadFSList() {

            var fd = new FormData();
            fd.append("request", "fetch_parish");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateFSData(res.result);
                    } else {
                        alert(res.result);
                    }

                    // console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateFSData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var fs_id = datas[i]["fs_id"];
                    var fs_site = datas[i]["fs_site"];

                    tmpl += "<option value="+ fs_site +" data-id="+ fs_id +">"+ fs_site +"</option>";

                }
                
            } else {
                alert("No Result");
                $("#site_list").find("option").remove().end();
            }

            $("#site_list").append(tmpl);
            
        }


    });

</script>