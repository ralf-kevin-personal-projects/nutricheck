<?php include '../headers/dashboard-header.php'; ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Project Performance</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">PM Reports</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Project Performance</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="ecommerce-widget">

                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Total Feeding Sites</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 id="count_fs" class="mb-1">0</h1>
                                </div>
                                <!-- <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                    <span><i class="fa fa-fw fa-arrow-up"></i></span><span>5.86%</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Total Beneficiaries</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 id="count_b" class="mb-1">0</h1>
                                </div>
                                <!-- <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                    <span><i class="fa fa-fw fa-arrow-up"></i></span><span>5.86%</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Total Male</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 id="count_b_m" class="mb-1">0</h1>
                                </div>
                                <!-- <div class="metric-label d-inline-block float-right text-primary font-weight-bold">
                                    <span>N/A</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">Total Female</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 id="count_b_fm" class="mb-1">0</h1>
                                </div>
                                <!-- <div class="metric-label d-inline-block float-right text-primary font-weight-bold">
                                    <span>N/A</span>
                                </div> -->
                            </div>
                        </div>
                    </div>                    
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-muted">% Feeding Completion</h5>
                                <div class="metric-value d-inline-block">
                                    <h1 id="count_fc" class="mb-1">0%</h1>
                                </div>
                                <!-- <div class="metric-label d-inline-block float-right text-secondary font-weight-bold">
                                    <span>-2.00%</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- ============================================================== -->
                
                    <!-- ============================================================== -->

                                    <!-- recent orders  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-8 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Program Scale</h5>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table id="tbl-ps" class="table">
                                        <thead class="bg-light">
                                            <tr class="border-0">
                                                <th class="border-0">Feeding Sites</th>
                                                <th class="border-0">Beneficiaries</th>
                                                <th class="border-0">Male</th>
                                                <th class="border-0">Female</th>
                                            </tr>
                                        </thead>
                                        <!-- <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>1</td>
                                                <td>Product #1 </td>
                                                <td>id000001 </td>
                                            </tr>
                                            <tr> -->
                                                <td>2</td>
                                                <td>1</td>
                                                <td>Product #2 </td>
                                                <td>id000002 </td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end recent orders  -->


                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- customer acquistion  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Feeding Completion</h5>
                            <div class="card-body">


                                <div class="bar-chart">

                                    <div id="site_fc" class="chart clearfix">

                                        <!-- <div class="item">
                                            <div class="bar">
                                                <span class="percent">51%</span>
                                
                                                <div class="item-progress" data-percent="51">
                                                    <span class="title">Site 1</span>
                                                </div>

                                            </div>

                                        </div> -->



                                        
                                    </div>
                                    <!-- //.chart -->
                                </div>



                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end customer acquistion  -->
                    <!-- ============================================================== -->
                    
                </div>

            </div>



            <!-- DEMOGRAPHICS -->
            <div class="ecommerce-widget">
                <div class="row">
                
                    <!-- ============================================================== -->

                                    <!-- recent orders  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Demographics</h5>
                            <div class="card-body p-0">
                                    <div id="chartMale" style="height: 300px; width: 100%;"></div>
                                    <div id="chartFemale" style="height: 300px; width: 100%;"></div>                                    
                            </div>
                        </div>
                    </div>



                    <div class="col-xl-8 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Weight Status</h5>
                            <div class="card-body p-0">
                                <div id="chartWeight" style="height: 300px; width: 100%;"></div>                            
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Attendance Rate</h5>
                            <div class="card-body p-0">
                                <div id="chartAtt" style="height: 300px; width: 100%;"></div>                            
                            </div>
                        </div>
                    </div>



                    <div class="col-xl-8 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Height Status</h5>
                            <div class="card-body p-0">
                                <div id="chartHeight" style="height: 300px; width: 100%;"></div>                            
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Nutritional Rate</h5>
                            <div class="card-body p-0">
                                <div id="chartNR" style="height: 300px; width: 100%;"></div>                            
                            </div>
                        </div>
                    </div>                                  
                    <!-- ============================================================== -->
                    <!-- end recent orders  -->                    
                    
                </div>

            </div>




        </div>
    </div>


</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->

<?php include '../headers/dashboard-footer.php'; ?>

<script>

$(document).ready(function(){

    loadCountFS();
    loadCountBene();
    loadData();
    loadAgeData();
    // loadWeightData();
    loadNutriData();

    function loadCountFS() {

        var fd = new FormData();
        fd.append("request", "count_fs");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    $("#count_fs").text(res.result[0]["count"]);
                } else {
                    alert(res.result);
                }

                // console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }


    function loadCountBene() {

        var fd = new FormData();
        fd.append("request", "count_b");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {

                    $("#count_b").text(res.result[0]["count"]);
                    $("#count_b_m").text(res.result[0]["male"]);
                    $("#count_b_fm").text(res.result[0]["female"]);

                } else {
                    alert(res.result);
                }

                // console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }


    function loadData() {

        var fd = new FormData();
        fd.append("request", "fetch_ps");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    populateData(res.result);
                } else {
                    alert(res.result);
                }

                // console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }

    //load age
    function loadAgeData() {

        var fd = new FormData();
        fd.append("request", "fetch_age");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    populateAgeData(res.result);
                } else {
                    alert(res.result);
                }
                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });

    }

    //load weight to be continue
    function loadWeightData() {

        var fd = new FormData();
        fd.append("request", "fetch_weight");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    populateAgeData(res.result);
                } else {
                    alert(res.result);
                }
                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });

    }

    function loadNutriData() {
        
        var fd = new FormData();
        fd.append("request", "fetch_weight");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    populateNutriData(res.result);
                } else {
                    alert(res.result);
                }
                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }



    function populateData(datas) {

        var tmpl;
        var tmplFC = "";
        var sumFC = 0;
        var countFC = 0;
        var totalFC = 0;

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                var fs_id = datas[i]["fs_id"];
                var fs_site = datas[i]["fs_site"];
                var count_b = datas[i]["count_b"];
                var male = datas[i]["male"];
                var female = datas[i]["female"];
                var fs_date_started = datas[i]["fs_date_started"];
                var fs_date_finish = datas[i]["fs_date_finish"];
                
                var fcBar = setFeedingCompletion(fs_date_started, fs_date_finish);

                tmpl += "<tr>"+
                        "<td>"+ fs_site +"</td>"+
                        "<td>"+ count_b +"</td>"+
                        "<td>"+ male +"</td>"+
                        "<td>"+ female +"</td>"+
                        // "<td>"+ fs_date_started +"</td>"+
                        // "<td>"+ fs_date_finish +"</td>"+
                        "</tr>";

                sumFC += parseFloat(setFeedingCompletion(fs_date_started, fs_date_finish));

                tmplFC += 
                        "<div class='item'>"+
                            "<div class='bar'>"+
                                "<span class='percent'>"+ fcBar +"%</span>"+
                            
                                "<div class='item-progress' data-percent='"+ fcBar +"'>"+
                                    "<span class='title'>"+ fs_site +"</span>"+
                                "</div>"+

                            "</div>"+
                        "</div>";                        


                countFC++;

                
            }
            
            totalFC = sumFC / countFC;
            $("#count_fc").text(totalFC.toString().substr(0,4) + "%" );

        } else {
            alert("No Result");
        }

        $("#tbl-ps").find("tbody tr").remove().end();
        $("#tbl-ps").append(tmpl);
        $("#tbl-ps").DataTable();

        $("#site_fc").append(tmplFC);
        // for feeding completion charts
        barChart();
        
    }

    function setFeedingCompletion(start, end) {
        Date.prototype.getCurrentDate = (function(){
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });

        var c = new Date().getCurrentDate();
        var curr = new Date(c).getTime();
        var start = new Date(start).getTime();
        var end = new Date(end).getTime();
        
        var complete = ((curr - start) / (end - start) ) * 100;

        return complete.toFixed(2);
    }

    function populateAgeData(datas) {

        var tmpl;
        var agesM = [];
        var agesF = [];

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                var b_age = datas[i]["b_age"];
                var b_gender = datas[i]["b_gender"];
                var count = datas[i]["count"];

                if (b_gender == "Male") {
                    agesM.push({
                        label: "Age " + b_age, 
                        y: parseInt(count)
                    });
                } else {
                    agesF.push({
                        label: "Age " + b_age, 
                        y: parseInt(count)
                    });
                }

            }
            
        } else {
            alert("No Result");
        }

        //FOR PLOTTING MALE DEMO
        setInterval(function(){

            //Better to construct options first and then pass it as a parameter
            var optionsM = {
                title: {
                    text: "Male by Age",
                    fontSize: 20            
                },
                dataPointWidth: 30,        
                data: [              
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: agesM
                }
                ]
            };
            
            $("#chartMale").CanvasJSChart(optionsM);


        }, 1000);


        //FOR PLOTTING MALE DEMO
        setInterval(function(){

            //Better to construct options first and then pass it as a parameter
            var optionsFM = {
                title: {
                    text: "Female by Age",
                    fontSize: 20            
                },
                dataPointWidth: 30,        
                data: [              
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: agesF
                }
                ]
            };
            console.log(agesF)
            $("#chartFemale").CanvasJSChart(optionsFM);

        }, 1500);



    }

    //to be continue
    function populateWeightData(datas) {

        var tmpl;
        var agesM = [];

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                var b_bmi_status_new = datas[i]["b_bmi_status_new"];
                var b_weight_new = datas[i]["b_weight_new"];
                var count = datas[i]["count"];

                if (b_gender == "Male") {
                    agesM.push({
                        label: "Age " + b_age, 
                        y: parseInt(count)
                    });
                } else {
                    agesF.push({
                        label: "Age " + b_age, 
                        y: parseInt(count)
                    });
                }

            }
            
        } else {
            alert("No Result");
        }

        //FOR PLOTTING MALE DEMO
        setInterval(function(){

            //Better to construct options first and then pass it as a parameter
            var optionsM = {
                title: {
                    text: "Male by Age",
                    fontSize: 20            
                },
                dataPointWidth: 30,        
                data: [              
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: agesM
                }
                ]
            };
            
            $("#chartMale").CanvasJSChart(optionsM);


        }, 1000);


        //FOR PLOTTING MALE DEMO
        setInterval(function(){

            //Better to construct options first and then pass it as a parameter
            var optionsFM = {
                title: {
                    text: "Female by Age",
                    fontSize: 20            
                },
                dataPointWidth: 30,        
                data: [              
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: agesF
                }
                ]
            };
            console.log(agesF)
            $("#chartFemale").CanvasJSChart(optionsFM);

        }, 1500);



    }

    //to be continue
    function populateNutriData(datas) {

        var tmpl;
        var nutri = [];
        var totalBeneficiary = 0;

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                var b_bmi_status_new = datas[i]["b_bmi_status_new"];
                var b_weight_new = datas[i]["b_weight_new"];
                var count = parseInt(datas[i]["count"]);
                
                nutri.push({
                    label: b_bmi_status_new , y: count
                });
            }
            

        } else {
            alert("No Result");
        }

        //FOR PLOTTING MALE DEMO
        // setInterval(function(){
                    
            var optionsNR = {
                animationEnabled: true,
                title: {
                    // text: "ACME Corporation Apparel Sales"
                },
                data: [{
                    type: "doughnut",
                    innerRadius: "50%",
                    // showInLegend: true,
                    // legendText: "{label}",
                    // indexLabel: "{label}: #percent%",
                    dataPoints: nutri
                }]
            };

            $("#chartNR").CanvasJSChart(optionsNR);


        // }, 1000);


    }












    $(window).resize(function(){
        barChart();
    });
    
    function barChart(){
        $('.bar-chart').find('.item-progress').each(function(){
            var itemProgress = $(this),
            itemProgressWidth = $(this).parent().width() * ($(this).data('percent') / 100);
            itemProgress.css('width', itemProgressWidth);
        });
    };












    var optionsAtt = {
        animationEnabled: true,
        title: {
            // text: "ACME Corporation Apparel Sales"
        },
        data: [{
            type: "doughnut",
            innerRadius: "50%",
            // showInLegend: true,
            // legendText: "{label}",
            // indexLabel: "{label}: #percent%",
            dataPoints: [
                { label: "Attended days: "+ "80%" , y: 80 },
                { label: "", y: 10 }
            ]
        }]
    };

    

    var optionsW = {
        animationEnabled: true,
        theme: "light2",
        title:{
            // text: "Actual vs Projected Sales",
            // fontSize: 20
        },
        axisX:{
            title: "Months",            
            valueFormatString: "#"
        },
        axisY: {
            title: "Weight (kg)",
            suffix: "",
            minimum: 0
        },
        toolTip:{
            shared:true
        },  
        legend:{
            cursor:"pointer",
            verticalAlign: "bottom",
            horizontalAlign: "left",
            dockInsidePlotArea: true,
            itemclick: toogleDataSeries
        },
        data: [{
            type: "line",
            showInLegend: true,
            name: "Severely Underweight",
            markerType: "square",
            xValueFormatString: "#",
            color: "#F08080",
            yValueFormatString: "",
            dataPoints: [
                { x: 1, y: 100 },
                { x: 2, y: 150 },
                { x: 3, y: 160 },
                { x: 4, y: 190 },
                { x: 5, y: 180 },
                { x: 6, y: 150 }
            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "Underweight",
            markerType: "square",            
            xValueFormatString: "#",
            // lineDashType: "dash",
            yValueFormatString: "",
            dataPoints: [
                { x: 1, y: 120 },
                { x: 2, y: 100 },
                { x: 3, y: 200 },
                { x: 4, y: 30 },
                { x: 5, y: 150 },
                { x: 6, y: 300 }
            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "Normal",
            markerType: "square",            
            color: "#aaa",            
            xValueFormatString: "#",
            // lineDashType: "dash",
            yValueFormatString: "",
            dataPoints: [
                { x: 1, y: 200 },
                { x: 2, y: 15 },
                { x: 3, y: 22 },
                { x: 4, y: 233 },
                { x: 5, y: 150 },
                { x: 6, y: 240 }
            ]
        }
        ]
    };



    $("#chartWeight").CanvasJSChart(optionsW);
    $("#chartAtt").CanvasJSChart(optionsAtt);
    $("#chartHeight").CanvasJSChart(optionsW);

    function toogleDataSeries(e){
        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else{
            e.dataSeries.visible = true;
        }
        e.chart.render();
    }    

});

</script>































































































































