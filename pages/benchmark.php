<?php include '../headers/dashboard-header.php'; ?>

<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="dashboard-ecommerce">
        <div class="container-fluid dashboard-content ">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h2 class="pageheader-title">Benchmark</h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Site Performance</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Benchmark</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="ecommerce-widget">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="offset-lg-3 col-lg-5">
                                        <label>Date: </label>
                                        <input id="date_started" type="date" class="form-control" />                                                                                    
                                    </div>
                                    <div class="col-lg-2">
                                        <label>&emsp;</label>
                                        <button id="btn_search" class="form-control btn btn-md btn-primary">SEARCH</button>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>                  
                </div>

                <div id="result">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label><b>Date Started:</b></label>
                                            <label id="lbl_date_start" >12.09.2018 </label>
                                        </div>
                                        <div class="col-lg-6">
                                            <label><b>Feeding Completion:</b></label>
                                            <label id="lbl_fc">0%</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <label><b>Date End:</b></label>
                                            <label id="lbl_date_end" >12.09.2018 </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                  
                    </div>            

                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <div class="table-responsive">
                                            <table id="tbl-b" class="table">
                                                <thead class="bg-light">
                                                    <tr class="border-0">
                                                        <th class="border-0">Beneficiaries</th>
                                                        <th class="border-0">Male</th>
                                                        <th class="border-0">Female</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="m-r-10"><img src="assets/images/product-pic.jpg" alt="user" class="rounded" width="45"></div>
                                                        </td>
                                                        <td>Product #1 </td>
                                                        <td>id000001 </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                        </div>                  
                    </div>


                    <div class="row">
                        <!-- ============================================================== -->
                    
                        <!-- ============================================================== -->

                                        <!-- recent orders  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-8 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Weight Status</h5>
                                <div class="card-body p-0">
                                    <div id="chartWeight" style="height: 300px; width: 100%;"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Attendance Rate</h5>
                                <div class="card-body">


                                    <div class="bar-chart">

                                        <div class="chart clearfix">

                                            <div class="item">
                                                <div class="bar">
                                                    <span class="percent">51%</span>
                                    
                                                    <div class="item-progress" data-percent="51">
                                                        <!-- <span id="site_name" class="title">Site 1</span> -->
                                                    </div>
                                                    <!-- //.item-progress -->
                                                </div>
                                                <!-- //.bar -->
                                            </div>
                                            <!-- //.item -->

                                        </div>
                                        <!-- //.chart -->
                                    </div>



                                </div>
                            </div>
                        </div>


                        <div class="col-xl-8 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Height Status</h5>
                                <div class="card-body p-0">
                                    <div id="chartHeight" style="height: 300px; width: 100%;"></div>                            
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Nutritional Rate</h5>
                                <div class="card-body p-0">
                                    <div id="chartNR" style="height: 300px; width: 100%;"></div>                            
                                </div>
                            </div>
                        </div>


                        <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Demographics</h5>
                                <div class="card-body p-0">
                                        <div id="chartMale" style="height: 300px; width: 100%;"></div>
                                        <div id="chartFemale" style="height: 300px; width: 100%;"></div>                                    
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row">



                    </div>


                </div>


            </div>

    </div>

</div>
<!-- ============================================================== -->
<!-- end wrapper  -->
<!-- ============================================================== -->

<?php include '../headers/dashboard-footer.php'; ?>










<script>

$(document).ready(function(){

    // init();

    $(document).on("click", "#btn_search", function(){

        var date_started_key = "#date_started";
        var date_started = $(date_started_key).val();

        var values = [date_started];
        var keys = [date_started_key];

        alert("fs: " + localStorage.getItem("fs_id"));

        if (validateItems(values, keys)) {
            parishRequest(values, "fetch_parish");
        } else {
            alert("Please input all the empty fields");
        }
        
    });


    function parishRequest(value, type) {

        var fd = new FormData();
        fd.append("id", localStorage.getItem("fs_id"));
        fd.append("date_started", value[0]);
        fd.append("request", "parish_perf");

        // alert(localStorage.getItem("fs_id"));

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    populateData(res.result);
                } else {
                    alert(res.result);
                }

                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }




    function populateData(datas) {

        var tmpl;
        var hasValue = false;
        var feedingCompletion;

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                if (datas[i]["fs_id"] != null) {

                    var fs_id = datas[i]["fs_id"];
                    var fs_parish = datas[i]["fs_parish"];
                    var fs_date_started = datas[i]["fs_date_started"];
                    var fs_date_finish = datas[i]["fs_date_finish"];
                    var male = datas[i]["male"];
                    var female = datas[i]["female"];
                    var count_b = datas[i]["count_b"];

                    tmpl += "<tr>"+
                            "<td>"+ count_b +"</td>"+
                            "<td>"+ male +"</td>"+
                            "<td>"+ female +"</td>"+
                            "</tr>";

                    feedingCompletion = setFeedingCompletion(fs_date_started, fs_date_finish);
                    $("#lbl_date_start").text(fs_date_started);
                    $("#lbl_date_end").text(fs_date_finish);
                    $("#lbl_fc").text(feedingCompletion + "%");

                    hasValue = true;
                } else {
                    alert("No Result");
                }



            }
            
        } else {
            alert("No Result");
        }

        if (hasValue) {
            $("#result").show();
            $("#tbl-b").find("tbody tr").remove().end();
            $("#tbl-b").append(tmpl);
            $('#tbl-b').DataTable();

            barChart();
        } else {
            $("#result").hide();
        }


        // for populating attendance
        var optionsAtt = {
            animationEnabled: true,
            title: {
                // text: "ACME Corporation Apparel Sales"
            },
            data: [{
                type: "doughnut",
                innerRadius: "50%",
                // showInLegend: true,
                // legendText: "{label}",
                // indexLabel: "{label}: #percent%",
                dataPoints: [
                    { label: "Attended days: "+ feedingCompletion +"%" , y: 80 },
                    { label: "", y: 10 }
                ]
            }]
        };

        // $("#chartMale").CanvasJSChart(optionsM);
        // $("#chartFemale").CanvasJSChart(optionsFM);
        // $("#chartWeight").CanvasJSChart(optionsW);
        // $("#chartHeight").CanvasJSChart(optionsW);
        // $("#chartNR").CanvasJSChart(optionsAtt);        
    }


    function validateItems(values, keys) {

        var isNotEmpty = false;
        var cTrue = 0;
        var cFalse = 0;

        for (var i = 0; i < values.length; i++) {

            if (values[i] == "" || values[i] == null) {
                $(keys[i]).addClass("border-danger");

                cFalse++;
            } else {

                cTrue++;
            }
        }

        if (cTrue > cFalse) {
            isNotEmpty = true;
        } else {
            isNotEmpty = false;
        }

        return isNotEmpty;

    }

    function init() {
        $("#result").hide();
    }

    function setFeedingCompletion(start, end) {
        Date.prototype.getCurrentDate = (function(){
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });

        var c = new Date().getCurrentDate();
        var curr = new Date(c).getTime();
        var start = new Date(start).getTime();
        var end = new Date(end).getTime();
        
        var complete = ((curr - start) / (end - start) ) * 100;

        return complete.toFixed(2);
    }





    //For Charts
    barChart();
    
    $(window).resize(function(){
        barChart();
    });
    
    function barChart(){
        $('.bar-chart').find('.item-progress').each(function(){
            var itemProgress = $(this),
            itemProgressWidth = $(this).parent().width() * ($(this).data('percent') / 100);
            itemProgress.css('width', itemProgressWidth);
        });
    };





    //Better to construct options first and then pass it as a parameter
    var optionsM = {
        title: {
            text: "Male by Age",
            fontSize: 20            
        },
        dataPointWidth: 30,        
        data: [              
        {
            // Change type to "doughnut", "line", "splineArea", etc.
            type: "column",
            dataPoints: [
                { label: "Age 10",  y: 5  },
                { label: "Age 12", y: 15  },
                { label: "Age 13", y: 25  },
                { label: "Age 14",  y: 30  },
                { label: "Age 16",  y: 100  }
            ]
        }
        ]
    };

    //Better to construct options first and then pass it as a parameter
    var optionsFM = {
        title: {
            text: "Female by Age",
            fontSize: 20            
        },
        dataPointWidth: 30,        
        data: [              
        {
            // Change type to "doughnut", "line", "splineArea", etc.
            type: "column",
            dataPoints: [
                { label: "Age 10",  y: 5  },
                { label: "Age 12", y: 15  },
                { label: "Age 13", y: 25  },
                { label: "Age 14",  y: 30  },
                { label: "Age 16",  y: 100  }
            ]
        }
        ]
    };


    var optionsW = {
        animationEnabled: true,
        theme: "light2",
        title:{
            // text: "Actual vs Projected Sales",
            // fontSize: 20
        },
        axisX:{
            title: "Months",            
            valueFormatString: "#"
        },
        axisY: {
            title: "Number of Sales",
            suffix: "",
            minimum: 0
        },
        toolTip:{
            shared:true
        },  
        legend:{
            cursor:"pointer",
            verticalAlign: "bottom",
            horizontalAlign: "left",
            dockInsidePlotArea: true,
            itemclick: toogleDataSeries
        },
        data: [{
            type: "line",
            showInLegend: true,
            name: "Severely Underweight",
            markerType: "square",
            xValueFormatString: "#",
            color: "#F08080",
            yValueFormatString: "",
            dataPoints: [
                // { x: new Date(2017, 10, 1), y: 63 },
                // { x: new Date(2017, 10, 2), y: 69 },
                // { x: new Date(2017, 10, 3), y: 65 },
                // { x: new Date(2017, 10, 4), y: 70 },
                // { x: new Date(2017, 10, 5), y: 71 },
                // { x: new Date(2017, 10, 6), y: 65 },
                // { x: new Date(2017, 10, 7), y: 73 },
                // { x: new Date(2017, 10, 8), y: 96 },
                // { x: new Date(2017, 10, 9), y: 84 },
                // { x: new Date(2017, 10, 10), y: 85 },
                // { x: new Date(2017, 10, 11), y: 86 },
                // { x: new Date(2017, 10, 12), y: 94 },
                // { x: new Date(2017, 10, 13), y: 97 },
                // { x: new Date(2017, 10, 14), y: 86 },
                // { x: new Date(2017, 10, 15), y: 89 }
                { x: 1, y: 100 },
                { x: 2, y: 150 },
                { x: 3, y: 160 },
                { x: 4, y: 190 },
                { x: 5, y: 180 },
                { x: 6, y: 150 }
            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "Underweight",
            markerType: "square",            
            xValueFormatString: "#",
            // lineDashType: "dash",
            yValueFormatString: "",
            dataPoints: [
                { x: 1, y: 120 },
                { x: 2, y: 100 },
                { x: 3, y: 200 },
                { x: 4, y: 30 },
                { x: 5, y: 150 },
                { x: 6, y: 300 }
            ]
        },
        {
            type: "line",
            showInLegend: true,
            name: "Normal",
            markerType: "square",            
            color: "#aaa",            
            xValueFormatString: "#",
            // lineDashType: "dash",
            yValueFormatString: "",
            dataPoints: [
                { x: 1, y: 200 },
                { x: 2, y: 15 },
                { x: 3, y: 22 },
                { x: 4, y: 233 },
                { x: 5, y: 150 },
                { x: 6, y: 240 }
            ]
        }
        ]
    };

    // FOR THE MEANTIME
    $("#chartMale").CanvasJSChart(optionsM);
    $("#chartFemale").CanvasJSChart(optionsFM);
    $("#chartWeight").CanvasJSChart(optionsW);
    $("#chartHeight").CanvasJSChart(optionsW);
    $("#chartNR").CanvasJSChart(optionsAtt);     

    function toogleDataSeries(e){
        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else{
            e.dataSeries.visible = true;
        }
        e.chart.render();
    } 

});

</script>